#!/bin/bash

classifier=$1;
###
###  Small release script made by Mitchell
###  Takes care of everything, or at least it tried to. Feel free to expand it!
###
###  Just answer the questions honorably and truthfully, without wagging your tail, and you should be fine!

## Helper functions

function load_project_dir {
   project_dir="$(pwd)";
   log "Found project directory to be: ${project_dir}";
    git config --global user.email "ci@insidion.com";
    git config --global user.name "Insidion CI";
}

function log {
    echo "${1}" | tee -a "${project_dir}/release.log"
}

function read_versions {
    cd "${project_dir}";
    release_version="$(mvn -Dexec.executable='echo' -Dexec.args='${project.version}' --non-recursive exec:exec -q)"
    a=( ${release_version//./ } )                   # replace points, split into array
    ((a[2]++))                              # increment revision (or other part)
    dev_version="${a[0]}.${a[1]}.${a[2]}-SNAPSHOT"       # compose new version
     ((a[2]--))
    release_version="${a[0]}.${a[1]}.${a[2]}"       # compose new version
}

function check_versions {
    if [ -n "$release_version" ] && [ -n "$dev_version" ]; then
        log "Release version will be: ${release_version}"
        log "Development version will be: ${dev_version}"
    else
        log "You have not chosen the versions correctly. Exiting!"
        exit 1;
    fi
}

function check_git_status {
    if [ -n "$(git diff-index HEAD --)" ]; then
        log "You have uncommitted changes! please commit them before releasing."
        exit 1;
     fi
}

function set_versions {
    if mvn versions:set -DnewVersion=${1}; then
        log "Versions updated to ${1}";
    else
        log "Maven version set failed!";
        exit 1;
    fi
}

function git_commit {
    if ! git commit -am "${1}"; then
        log "Git commit failed";
    else
        log "Committed changes with message ${1}";
    fi
}


function git_tag {
    if ! git tag -a v${1} -m "Version ${1}"; then
        log "Git tag failed";
    else
        log "Tagged v${1}";
    fi
}

function git_merge_to_develop {
    git checkout develop;
    git merge master;
    git_push develop;
}

function git_push {
    cd "${project_dir}";
    bash  ./git-push.sh git@gitlab.com:Morlack/deployitnow.git "${1}";
}

function build {
  if mvn clean install -DskipTests -B; then
    log "Mvn clean install passed with flying colors";
  else
    log "Mvn clean install failed!";
    exit 1;
  fi
}

function deploy {
    docker build -t $REPOSITORY_URL:${1} .;
    docker push $REPOSITORY_URL:${1}
}

function clean {
    rm -r **/*.versionsBackup
}

# Main script
load_project_dir;
read_versions;
check_versions;

echo "Releasing for production!";
set_versions ${release_version};
echo "${release_version}" > version.txt
build;
deploy "${release_version}";
git_commit "Release version: ${release_version}";
git_tag ${release_version};
git_push ${release_version};

set_versions "${dev_version}";

cd "${project_dir}"
git_commit "New development version: ${dev_version}";
git_push develop;
log "Successfully release version ${release_version}!";

clean;
exit 0;
