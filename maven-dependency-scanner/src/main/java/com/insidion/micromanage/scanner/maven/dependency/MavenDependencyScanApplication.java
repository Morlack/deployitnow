package com.insidion.micromanage.scanner.maven.dependency;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.cli.MavenCli;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@SpringBootApplication
@Slf4j
public class MavenDependencyScanApplication implements CommandLineRunner {
    private final Map<File, PomRepresentation> before = new HashMap<>();
    private final Environment environment;
    private final RestTemplate restTemplate = new RestTemplate();

    public MavenDependencyScanApplication(final Environment environment) {
        this.environment = environment;
        restTemplate.setMessageConverters(Arrays.asList(new MappingJackson2HttpMessageConverter()));
    }

    public static void main(String[] args) {
        SpringApplication.run(MavenDependencyScanApplication.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {
        final String micromanageBaseUrl = environment.getProperty("MICROMANAGE_BASE_URL");
        final String micromanageProjectId = environment.getProperty("MICROMANAGE_PROJECT_ID");
        log.info("Starting dependency scanner with micromanage baseUrl: {} and project id: {}", micromanageBaseUrl, micromanageProjectId);
        XmlMapper xmlMapper = (XmlMapper) new XmlMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        MavenCli cli = new MavenCli();
        final String workingDirectory = System.getProperty("user.dir");
        readBeforeContents(xmlMapper, workingDirectory);
        System.setProperty("maven.multiModuleProjectDirectory", workingDirectory);
        System.setProperty("maven.version.rules", micromanageBaseUrl + "/maven-rules.xml");
        cli.doMain(new String[]{"versions:update-properties", "--batch-mode", "-Dexcludes=" + environment.getProperty("ARTIFACT_EXCLUDES")}, workingDirectory, System.out, System.out);
        cli.doMain(new String[]{"versions:use-latest-releases", "--batch-mode", "-Dexcludes=" + environment.getProperty("ARTIFACT_EXCLUDES")}, workingDirectory, System.out, System.out);
        cli.doMain(new String[]{"versions:update-parent", "--batch-mode"}, workingDirectory, System.out, System.out);

        List<DetectedFileChange> detectedChanges = new LinkedList<>();
        Files.walk(Paths.get(workingDirectory))
                .filter(Files::isRegularFile)
                .forEach((f) -> {
                    final String file = f.toString();
                    if (file.endsWith("pom.xml"))
                        try {
                            final PomRepresentation beforeRepresentation = before.get(f.toFile());
                            final String content = IOUtils.toString(f.toUri(), Charset.defaultCharset());
                            final PomRepresentation afterRepresentation = xmlMapper.readValue(content, PomRepresentation.class);
                            final String fileName = f.toString().replace(workingDirectory, "");
                            final List<Upgrade> upgrades = new LinkedList<>();
                            // compare parent
                            if (beforeRepresentation.parent != null && afterRepresentation.parent != null && !StringUtils.equalsIgnoreCase(beforeRepresentation.parent.version, afterRepresentation.parent.version)) {
                                upgrades.add(new Upgrade("parent", afterRepresentation.parent.groupId + ":" + afterRepresentation.parent.artifactId, beforeRepresentation.parent.version, afterRepresentation.parent.version));
                            }

                            // Compare properties
                            if (afterRepresentation.properties != null) {
                                afterRepresentation.properties.keySet().forEach(property -> {
                                    String before = beforeRepresentation.properties.get(property);
                                    String after = afterRepresentation.properties.get(property);
                                    if (!StringUtils.equalsIgnoreCase(before, after)) {
                                        log.info("Detected change for property {} from {} to {}", property, before, after);
                                        upgrades.add(new Upgrade("property", property, before, after));
                                    }
                                });
                            }

                            // Compare dependencies
                            upgrades.addAll(compareDependencies(beforeRepresentation.dependencies, afterRepresentation.dependencies, fileName, "dependency"));
                            if (beforeRepresentation.dependencyManagement != null) {
                                upgrades.addAll(compareDependencies(beforeRepresentation.dependencyManagement.dependencies, afterRepresentation.dependencyManagement.dependencies, fileName, "dependencyManagement"));
                            }

                            if (upgrades.size() > 0) {
                                final String base64Content = Base64.getEncoder().encodeToString(content.getBytes());
                                detectedChanges.add(new DetectedFileChange(fileName, base64Content, upgrades));
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                });

        log.info("{}", detectedChanges);
        restTemplate.postForEntity(micromanageBaseUrl + "/api/tasks/dependency/result/" + micromanageProjectId + "/maven", detectedChanges, String.class);
    }

    private void readBeforeContents(final XmlMapper xmlMapper, final String workingDirectory) throws IOException {
        Files.walk(Paths.get(workingDirectory))
                .filter(Files::isRegularFile)
                .forEach((f) -> {
                    String file = f.toString();
                    if (file.endsWith("pom.xml"))
                        try {
                            final String shortFileName = file.replace(workingDirectory, "");
                            System.out.println(shortFileName + " found!");
                            before.put(f.toFile(), xmlMapper.readValue(f.toFile(), PomRepresentation.class));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                });
    }

    private List<Upgrade> compareDependencies(final List<PomRepresentation.Dependency> before, final List<PomRepresentation.Dependency> after, final String fileName, final String type) {
        if (before == null || after == null) {
            return Collections.emptyList();
        }
        List<Upgrade> detectedChanges = new LinkedList<>();

        before.forEach(beforeDep -> {
            final PomRepresentation.Dependency afterDep = after.stream().filter(a -> a.equals(beforeDep)).findFirst().orElseThrow(() -> new IllegalStateException("before dep without after dep"));
            if (!StringUtils.equals(beforeDep.version, afterDep.version)) {
                detectedChanges.add(new Upgrade(type, afterDep.groupId + ":" + afterDep.artifactId, beforeDep.version, afterDep.version));
            }
        });

        return detectedChanges;
    }

    public static class PomRepresentation {
        public Parent parent;
        public Map<String, String> properties;
        public DependencyManagement dependencyManagement;
        public List<Dependency> dependencies;

        public static class DependencyManagement {
            public List<Dependency> dependencies;
        }

        public static class Parent {
            public String groupId;
            public String artifactId;
            public String version;
        }

        @EqualsAndHashCode(exclude = "version")
        public static class Dependency {
            public String groupId;
            public String artifactId;
            public String version;
        }
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class DetectedFileChange {
        public String file;
        public String fileContentBase64;
        public List<Upgrade> upgrades;

    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Upgrade {
        private String type;
        private String identifier;
        private String currentVersion;
        private String availableVersion;
    }
}
