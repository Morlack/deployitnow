#!/bin/bash

classifier=$1;
###
###  Small release script made by Mitchell
###  Takes care of everything, or at least it tried to. Feel free to expand it!
###
###  Just answer the questions honorably and truthfully, without wagging your tail, and you should be fine!

## Helper functions

function load_project_dir {
   project_dir="$(pwd)";
   log "Found project directory to be: ${project_dir}";
    git config --global user.email "ci@insidion.com";
    git config --global user.name "Insidion CI";
}

function log {
    echo "${1}" | tee -a "${project_dir}/release.log"
}

function read_versions {
    cd "${project_dir}";
    release_version="$(mvn -Dexec.executable='echo' -Dexec.args='${project.version}' --non-recursive exec:exec -q)"
}

function check_versions {
    if [ -n "$release_version" ] ; then
        log "Release version will be: ${release_version}"
    else
        log "You have not chosen the versions correctly. Exiting!"
        exit 1;
    fi
}

function build {
  if mvn clean install -DskipTests -B; then
    log "Mvn clean install passed with flying colors";
  else
    log "Mvn clean install failed!";
    exit 1;
  fi
}

function deploy {
    docker build -t $REPOSITORY_URL:${1} .;
    docker push $REPOSITORY_URL:${1}
}

function clean {
    rm -r **/*.versionsBackup
}

# Main script
load_project_dir;
read_versions;
check_versions;

echo "Releasing for SNAPSHOT!";
echo "${release_version}" > version.txt
build;
deploy "${release_version}";

log "Successfully release version ${release_version}!";

clean;
exit 0;
