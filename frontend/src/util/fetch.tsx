
export const put = (url: string, body: any) => {
    return fetch(url, {
        method: 'put',
        body: JSON.stringify(body),
        headers: {'Content-Type': 'application/json'}
    })
}

export const post = (url: string, body: any) => {
    return fetch(url, {
        method: 'post',
        body: JSON.stringify(body),
        headers: {'Content-Type': 'application/json'}
    })
}
