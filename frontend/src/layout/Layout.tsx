import React, {useEffect} from "react";
import {useSelector} from "react-redux";
import {loggedInSelector} from "../redux/slices/UserSlice";
import Header from "./Header";
import Pages from "./Pages";
import Login from "./Login";

function Layout() {
    const loggedIn = useSelector(loggedInSelector);
    // const history = useHistory();
    useEffect(() => {
        const redirect = localStorage.getItem('mm-login-redirect')
        if(loggedIn && redirect) {
            // TODO: Restore URL
        }
    }, [loggedIn])

    return (
        <div>
            <Header/>
            {loggedIn && <Pages/>}
            {!loggedIn && <Login/>}
        </div>
    );
}

export default Layout;
