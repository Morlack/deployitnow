import React, {useEffect} from "react";
import {Changelog} from "../components/common/Changelog";
import {useLocation} from "react-router-dom";

function Login() {
    const location = useLocation();
    useEffect(() => {
        localStorage.setItem('mm-login-redirect', JSON.stringify(location))
    }, [location])

    return (
        <div className="home-container uk-flex">
            <div className="uk-padding uk-flex-auto">

                <div className='uk-card uk-card-default uk-margin-bottom'>
                    <div className="uk-card-header">
                        <h3>About</h3>
                    </div>
                    <div className="uk-card-body">
                        <div className="uk-flex">
                            <div className="uk-flex-auto">
                                    <h3>Welkom bij MicroManage!</h3>

                                <p>Deze applicatie is een proof-of-concept om microservices in een complex landschap beter te kunnen beheren en onderhouden. Hier wordt actief
                                    aan ontwikkeld en de toolbox zal worden uitgebreid. Momenteel draait dit als proef bij HaMIS om te kijken of dit voordelen oplevert.</p>

                                <p><a href="https://trello.com/b/xMK2GHfV/micromanage">Op Trello wordt de ontwikkeling en feature requests bijgehouden.</a> Voor nieuwe feature
                                    requests, neem contact op met Mitchell Herrijgers van HaMIS. </p>

                                <p>Ik hoor uiteraard ook graag wat je ervan vindt! </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="uk-flex-auto uk-padding">
                <div className='uk-card uk-card-default uk-margin-bottom'>
                    <div className="uk-card-header">
                        <h3>Login</h3>
                    </div>
                    <div className="uk-card-body">
                        <a className="uk-button uk-button-primary" href="/oauth2/authorization/gitlab">Authorize using gitlab</a>
                    </div>
                </div>

                <Changelog/>
            </div>
        </div>
    );
}

export default Login;
