import React from "react";
import {Route} from 'react-router-dom'
import OverviewPageContainer from "../components/pages/OverviewPageContainer";
import DeployPageContainer from "../components/project/DeployPageContainer";
import ProjectPageContainer from "../components/project/ProjectPageContainer";
import UpgradePageContainer from "../components/upgrades/UpgradePageContainer";
import {Archive} from "../components/archive/Archive";
import {GraphPage} from "../components/graph/GraphPage";
import {SettingsPage} from "../components/settings/SettingsPage";
import Guide from "../components/common/Guide";

function Pages() {
    return (
        <div>
                <Route path={"/overview"} component={OverviewPageContainer}/>
                <Route path={"/project/:id/deploy"} component={DeployPageContainer}/>
                <Route path={"/projects"} exact component={ProjectPageContainer}/>
                <Route path={"/projects/:id"} component={ProjectPageContainer}/>
                <Route path={"/upgrades"} component={UpgradePageContainer}/>
                <Route path={"/archive"} component={Archive}/>
                <Route path={"/graph"} component={GraphPage}/>
                <Route path={"/settings"} component={SettingsPage}/>
                <Route path={"/guide"} component={Guide}/>
                <Route path={"/"} exact component={OverviewPageContainer}/>
        </div>
    );
}

export default Pages;
