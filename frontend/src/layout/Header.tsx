import React from "react";
import {Link} from 'react-router-dom'
import {useSelector} from "react-redux";
import {loggedInSelector} from "../redux/slices/UserSlice";

function Header() {
    const isLoggedIn = useSelector(loggedInSelector);
    return (
        <div className="uk-navbar-container" data-uk-navbar>
            <div className="uk-navbar-left">
                <a href="/" className="uk-navbar-item uk-logo">MicroManage</a>
                {isLoggedIn && <ul className="uk-navbar-nav">
                    <li><Link to="/">Overview</Link></li>
                    <li><Link to="/projects">Projects</Link></li>
                    <li><Link to="/archive">Archive</Link></li>
                    <li><Link to="/graph">Graph</Link></li>
                    <li><Link to="/upgrades">Upgrades</Link></li>
                </ul>}
            </div>
            <div className="uk-navbar-right">
                {isLoggedIn && <ul className="uk-navbar-nav uk-margin-right">
                    <li><Link to="/settings"><i className="fa fa-cog"/></Link></li>
                </ul>}
            </div>
        </div>
    );
}

export default Header;
