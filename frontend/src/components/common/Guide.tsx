import React from "react";


export default function Guide() {
    return <div className={"uk-container"}>
        <div className="uk-card uk-card-default uk-margin-top">
            <div className="uk-card-body">
                <h1>Micromanage Guide</h1>
                <p>Micromanage is easy to use. This guides serves as a starting point. We'll start with the fundamentals.</p>

                <h3>Fundamentals</h3>
                <p>
                    Micromanage is a tool built upon the gitlab API. It scans all gitlab projects that have deployment capabilities for new changelogs. A project is scanned every two minutes.
                </p>
                <div className="uk-flex">
                    <div className="uk-flex-1">
                        <p>The changelogs are scanned and parsed for a JIRA key. If there is none, it defaults back to the text provided. It matches these changelogs with those of other projects and versions, creating a cross-project list of required deployments for that feature. This is shown on the overview page for every feature, like is shown to the right</p>
                        <p>As you can see, Micromanage is also aware of the versions present on the different environments, creating an overview of the current deployment status. Clicking on one of the play buttons will take you to the deployment page.</p>
                        </div>
                    <img src="/guide_overview.png" alt="Overview of versions required for a feature" width={400} className="uk-margin-left"/>
                </div>

                <div className="uk-flex">
                    <div>
                        <img src="/guide_deploy.png" alt="Deploy page" width={500} className="uk-margin-left"/>
                    </div>
                    <div className="uk-flex-1 uk-margin-left">
                        <h3>Deployment page</h3>
                        <p>
                            After clicking the play button you will first visit the deployment page. This will show you exactly which features contains in which versions you will deploy. This list can be quite large if the project has not been deployed for a while.
                        </p>
                        <p>Use this page as you best friend. It will prevent you from putting features into production that are not ready yet.</p>
                    </div>
                </div>

                <h3>Troubleshooting</h3>
                <p>This section will discuss the most frequent issues.</p>

                <h4>The environment versions are out of date</h4>
                <p>The environment versions are updated by webhooks when a pipeline gets completed. It is also done every 30 minutes in addition to that. If something is out of date, you can refresh it manually on the project's page. </p>
                <p>You can reach this page by clicking on a project's name in the feature overview, or going to the projects page.</p>

                <h4>The JIRA-key is not detected properly, or the feature's name is not right</h4>
                <p>
                    In order to parse the changelogs of gitlab correctly, it should adhere to the following format: "[JIRA-KEY] Your issue name". You should limit one feature per line
                </p>


                <h3>Additional information</h3>
                <p>Which information would you like to see on this page? Let us know!</p>
            </div>
        </div>
    </div>
}
