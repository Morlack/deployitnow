import React from "react";


function Loading() {
    return <div className="uk-container uk-padding">
        <div className="uk-card uk-card-primary">
            <div className="uk-card-header">
                Loading...
            </div>

            <div className="uk-card-body">
                <i className="fa fa-spin fa-cog"/> We are loading the good stuff for you!
            </div>
        </div>
    </div>
}

export default Loading;
