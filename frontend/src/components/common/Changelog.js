import React from "react";

export class Changelog extends React.Component {
    state = {
        changelog: null
    }


    componentDidMount() {
        fetch("/changelog.json").then(async (response) => {
            this.setState({
                changelog: await response.json()
            })
        })
    }


    render() {
        return (
            <div className='uk-card uk-card-default'>
                <div className="uk-card-header">
                    <h3>Changelog</h3>
                </div>
                <div className="uk-card-body">
                    {this.renderChangelog()}
                </div>
            </div>
        )
    }

    renderChangelog() {
        if (!this.state.changelog) {
            return <></>
        }

        return <div>
            {this.state.changelog.inDevelopment.length > 0 && <div>
                <h4>Currently in development</h4>
                <ul>
                    {this.state.changelog.inDevelopment.map(item => <li key={item}>{item}</li>)}
                </ul>
            </div>}

            {this.state.changelog.versions.map(version => (
                <div key={version.version}>
                    <h4>{version.version}</h4>
                    <ul>
                        {version.entries.map(entry => <li key={entry}>{entry}</li>)}
                    </ul>
                </div>))}
        </div>
    }
}
