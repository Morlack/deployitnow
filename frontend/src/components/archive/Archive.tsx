import React, {useEffect, useState} from "react";
import Loading from "../common/Loading";
import FeatureComponent from "../feature/Feature";

/* eslint jsx-a11y/anchor-is-valid: "off" */
export function Archive() {
    const [page, setPage] = useState(0)
    const [items, setItems] = useState(null as any[] | null);

    useEffect(() => {
        fetch("/api/features/archive?page=" + page).then(async (response) => {
            setItems(await response.json());
        })
    }, [page])

    if(!items) {
        return <Loading/>
    }
    return <div className={"uk-container"}>
        <div className="uk-margin-top">
            <ul className="uk-pagination uk-margin">
                {page !== 0 && <li><a href="#" onClick={() => setPage(page - 1)}><span className="uk-margin-small-right" data-uk-pagination-previous></span> Previous</a></li>}
                <li className="uk-margin-auto-left" onClick={() => setPage(page + 1)}><a href="#">Next <span className="uk-margin-small-left" data-uk-pagination-next></span></a></li>
            </ul>
        </div>
        <div className="uk-margin-top">
        {items.map(item => {
            return <FeatureComponent feature={item}/>
        })}
        </div>
    </div>
}
