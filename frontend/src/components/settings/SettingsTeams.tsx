import React from "react";
import {useSelector} from "react-redux";
import {Team, teamsSelector} from "../../redux/slices/TeamsSlice";
import {post, put} from "../../util/fetch";
import {toast} from "react-toastify";
import {Formik} from "formik";


function TeamsForm({team}: { team?: Team }) {
    return <Formik
        initialValues={{
            name: team?.name ?? '',
            slackChannel: team?.slackChannel ?? '',
            depedencyCheckInterval: team?.depedencyCheckInterval ?? 14,
            notificationType: team?.notificationType ?? 'ALL',
        }}
        onSubmit={(values, {setSubmitting, resetForm}) => {
            if (team?.id) {
                put('/api/teams/' + team.id, {...values, id: team.id}).then(() => {
                    toast("Team saved!", {type: "success"})
                    setSubmitting(false)
                })
            } else {
                post('/api/teams', {...values}).then(() => {
                    toast("Team saved!", {type: "success"})
                    setSubmitting(false)
                    resetForm()
                })
            }
        }}
    >
        {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
          }) => (
            <form onSubmit={handleSubmit} className="uk-form">
                <div className="uk-margin">
                    <input className="uk-input" type="text" placeholder="Team name"
                           name="name"
                           value={values.name}
                           onChange={handleChange}/>
                </div>
                <div className="uk-margin">
                    <input className="uk-input" type="text" placeholder="Slack channel"
                           name="slackChannel"
                           value={values.slackChannel}
                           onChange={handleChange}/>
                </div>

                <fieldset className="uk-fieldset">
                    <legend className="uk-legend">Service maintenance</legend>
                    <div className="uk-margin">
                        <div className="uk-flex">
                            <div className="uk-flex-auto">
                                <div className="uk-form-label">Dependency update interval (days)</div>
                                <div className="uk-form-controls">
                                    <input className="uk-input" type="number" name="depedencyCheckInterval" value={values.depedencyCheckInterval} onChange={handleChange}/>
                                </div>
                            </div>
                            <div className="uk-flex-auto uk-margin-left">
                                <div className="uk-form-label">Notification settings</div>
                                <div className="uk-form-controls">
                                    <select className="uk-select" name="notificationType" value={values.notificationType} onChange={handleChange}>
                                        <option value="ALL">Notify on every update</option>
                                        <option value="CREATE">Notify on initial MR</option>
                                        <option value="NEVER">Never notify</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div className={"uk-margin"}>
                    <button type="submit" className="uk-button uk-button-primary" disabled={isSubmitting}>
                        {team?.id ? 'Update' : 'Create'} team
                    </button>
                </div>
            </form>
        )}
    </Formik>
}

/* eslint jsx-a11y/anchor-is-valid: "off" */
export function SettingsTeams() {
    const teams = useSelector(teamsSelector)

    return <div className="uk-flex">
        <div className="uk-flex-1 uk-margin-right">
            <ul data-uk-accordion>
                {teams.map(team => <li key={team.id}>
                    <a className="uk-accordion-title" href="#">{team.name}</a>
                    <div className="uk-accordion-content">
                        <TeamsForm team={team}/>
                    </div>
                </li>)}
            </ul>
        </div>
        <div className="uk-flex-1">
            <h4>Create a new team</h4>
            <TeamsForm/>
        </div>

    </div>
}
