import React from "react";
import {Field, Formik} from "formik";
import {useSelector} from "react-redux";
import {settingsSelector} from "../../redux/slices/SettingSlice";
import {toast} from "react-toastify";
import {put} from "../../util/fetch";

export function SettingsGitlab() {
    const settings = useSelector(settingsSelector)

    return <div>
        <Formik
            initialValues={{
                enabled: settings['GITLAB_ENABLED'] === 'true',
                enableProjectScan: settings['GITLAB_ENABLE_PROJECT_SCAN'] === 'true',
                enableUpgrader: settings['GITLAB_ENABLE_UPGRADER'] === 'true',
                enableDetector: settings['GITLAB_ENABLE_DETECTOR'] === 'true',
                url: settings['GITLAB_URL'],
                apiToken: settings['GITLAB_API_TOKEN'],
                envWhitelist: settings['GITLAB_ENVIRONMENTS_WHITELIST'],
            }}
            onSubmit={(values, {setSubmitting}) => {
                Promise.all([
                    put(`/api/settings/GITLAB_ENABLED`, {value: values.enabled}),
                    put(`/api/settings/GITLAB_ENABLE_PROJECT_SCAN`, {value: values.enableProjectScan}),
                    put(`/api/settings/GITLAB_ENABLE_UPGRADER`, {value: values.enableUpgrader}),
                    put(`/api/settings/GITLAB_ENABLE_DETECTOR`, {value: values.enableDetector}),
                    put(`/api/settings/GITLAB_URL`, {value: values.url}),
                    put(`/api/settings/GITLAB_API_TOKEN`, {value: values.apiToken}),
                    put(`/api/settings/GITLAB_ENVIRONMENTS_WHITELIST`, {value: values.envWhitelist}),
                ]).then((res) => {
                    if (res.filter(r => !r.ok).length > 0) {
                        toast("Settings could not be saved...", {type: 'error'})
                    } else {
                        toast("New settings were saved successfully", {type: 'success'})
                    }
                    setSubmitting(false);
                })
            }}
        >
            {({
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
              }) => (
                <form onSubmit={handleSubmit} className="uk-form">
                    <div className="uk-margin">
                        <label><Field type="checkbox" className="uk-checkbox" name="enabled"/> Enable gitlab</label>
                    </div>
                    <div className="uk-margin">
                        <label><Field type="checkbox" className="uk-checkbox" name="enableProjectScan"/> Scan projects every 15 minutes</label>
                    </div>
                    <div className="uk-margin">
                        <label><Field type="checkbox" className="uk-checkbox" name="enableDetector"/> Enable technology detector</label>
                    </div>
                    <div className="uk-margin">
                        <label><Field type="checkbox" className="uk-checkbox" name="enableUpgrader"/> Enable upgrader</label>
                    </div>
                    <div className="uk-form-controls uk-margin-top">
                        <div className="uk-form-label">
                            Gitlab instance url
                        </div>
                        <input
                            type="text"
                            name="url"
                            className="uk-input"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.url}
                        />
                    </div>
                    <div className="uk-form-controls uk-margin-top">
                        <div className="uk-form-label">
                            Gitlab API token
                        </div>
                        <input
                            type="text"
                            name="apiToken"
                            className="uk-input"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.apiToken}
                        />
                    </div>
                    <div className="uk-form-controls uk-margin-top">
                        <div className="uk-form-label">
                            Environment whitelist (comma-separated)
                        </div>
                        <input
                            type="text"
                            name="envWhitelist"
                            className="uk-input"
                            placeholder="ontw,test,acc,prod"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.envWhitelist}
                        />
                    </div>

                    <div className={"uk-margin"}>
                        <button type="submit" className="uk-button uk-button-primary" disabled={isSubmitting}>
                            Submit
                        </button>
                    </div>
                </form>
            )}
        </Formik>
    </div>
}
