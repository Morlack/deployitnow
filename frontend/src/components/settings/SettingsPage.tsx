import React from "react";
import {Link, Route, useRouteMatch} from "react-router-dom";
import {SettingsGeneral} from "./SettingsGeneral";
import {SettingsGitlab} from "./SettingsGitlab";
import {SettingsTeams} from "./SettingsTeams";

export function SettingsPage() {
    const match = useRouteMatch();

    return <div className="uk-container uk-margin-top">
        <div className="uk-card uk-card-default uk-padding-remo">
            <div className={"uk-card-body"}>
                <ul className="uk-tab">
                    <Route path={match.url} exact>{({match: childMatch}) => <li className={childMatch ? 'uk-active' : ''}><Link to={match.url}>General</Link></li>}</Route>
                    <Route path={match.url + '/teams'} exact>{({match: childMatch}) => <li className={childMatch ? 'uk-active' : ''}><Link to={match.url + '/teams'}>Teams</Link></li>}</Route>
                    <Route path={match.url + '/gitlab'} exact>{({match: childMatch}) => <li className={childMatch ? 'uk-active' : ''}><Link to={match.url + '/gitlab'}>Gitlab</Link></li>}</Route>

                </ul>

                <Route path={match.path} exact render={() => <SettingsGeneral/>}/>
                <Route path={match.path + '/teams'} render={() => <SettingsTeams/>}/>
                <Route path={match.path + '/gitlab'} render={() => <SettingsGitlab/>}/>
            </div>
        </div>
    </div>
}
