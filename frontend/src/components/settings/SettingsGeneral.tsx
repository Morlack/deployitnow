import React from "react";
import {Formik} from "formik";
import {useSelector} from "react-redux";
import {settingsSelector} from "../../redux/slices/SettingSlice";
import {toast} from "react-toastify";
import {put} from "../../util/fetch";

export function SettingsGeneral() {
    const settings = useSelector(settingsSelector)

    return <div>
        <Formik
            initialValues={{baseUrl: settings['GENERAL_BASE_URL'], archiveEnvironments: settings['GENERAL_ARCHIVE_ENVIRONMENTS']}}
            onSubmit={(values, {setSubmitting}) => {
                Promise.all([
                    put(`/api/settings/GENERAL_BASE_URL`, {value: values.baseUrl}),
                    put(`/api/settings/GENERAL_ARCHIVE_ENVIRONMENTS`, {value: values.archiveEnvironments}),
                ]).then((res) => {
                    if (res.filter(r => !r.ok).length > 0) {
                        toast("Settings could not be saved...", {type: 'error'})
                    } else {
                        toast("New settings were saved successfully", {type: 'success'})
                    }
                    setSubmitting(false);
                })
            }}
        >
            {({
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
              }) => (
                <form onSubmit={handleSubmit} className="uk-form">
                    <div className="uk-form-controls">
                        <div className="uk-form-label">
                            Base URL of MicroManage
                        </div>
                        <input
                            type="text"
                            name="baseUrl"
                            className="uk-input"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.baseUrl}
                        />

                    </div>
                    <div className="uk-form-controls uk-margin-top">
                        <div className="uk-form-label">
                            Archive environments, comma-separated list
                        </div>
                        <input
                            type="text"
                            name="archiveEnvironments"
                            className="uk-input"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.archiveEnvironments}
                        />

                    </div>

                    <div className={"uk-margin"}>
                        <button type="submit" className="uk-button uk-button-primary" disabled={isSubmitting}>
                            Submit
                        </button>
                    </div>
                </form>
            )}
        </Formik>
    </div>
}
