import React, {useCallback, useEffect, useState} from "react";
import Loading from "../common/Loading";
import * as _ from 'lodash';

/* eslint jsx-a11y/anchor-is-valid: "off" */
function UpgradePageContainer() {
    const [upgrades, setUpgrades] = useState([] as any[])
    const [selectedUpgrade, setSelectedUpgrade] = useState(null as any)

    const fetchUpgrades = useCallback(() => {
        fetch("/api/upgrades").then(async (response) => {
            setUpgrades(await response.json())
        })
    }, [])

    const closeMergeRequest = (projectId: number) => {
        fetch(`/api/upgrades/mr/${projectId}/close`, {method: 'POST'}).then(async (response) => {
            fetchUpgrades();
        });
    }

    useEffect(() => {
        fetchUpgrades();
    }, [fetchUpgrades])

    if (upgrades == null) {
        return <Loading/>
    }

    if (upgrades.length === 0) {
        return <div className="uk-container uk-padding">
            <div className="uk-card uk-card-body uk-card-default uk-margin">
                There are no pending upgrades. Hooray!
            </div>
        </div>
    }

    return <div>
        <div className="uk-padding uk-container">
            <div className="uk-card uk-card-default uk-card-body uk-margin-bottom">
                There are currently {upgrades.reduce((acc, n) => acc + n.upgrades.length, 0)} upgrades pending
                for {upgrades.length} services.
            </div>
            <div className="uk-flex">
                <div className="uk-flex-1">
                    <div className="uk-card uk-card-default uk-card-body">
                        <ul className="uk-nav uk-nav-default">
                            {_.sortBy(upgrades, u => 1 - u.upgrades.length).map(upgrade => <li key={upgrade.project.id}>
                                <a href="#" onClick={() => setSelectedUpgrade(upgrade)}>{upgrade.project.name} ({upgrade.upgrades.length})</a>
                            </li>)}
                        </ul>
                    </div>
                </div>
                <div className="uk-flex-1">
                    <div className="uk-container uk-margin-left">
                        <div className="uk-card uk-card-body uk-card-default uk-margin">
                            {!selectedUpgrade &&
                            <p>Select a project on the left</p>
                            }
                            {selectedUpgrade && <div>
                                <p>There are {selectedUpgrade.upgrades.length} upgrades required for this project.</p>
                                <table className="uk-table">
                                    <thead>
                                    <tr>
                                        <th>File</th>
                                        <th>Type</th>
                                        <th>Dependency</th>
                                        <th>Current version</th>
                                        <th>Newest version</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {selectedUpgrade.upgrades.map((upgrade: any) => (<tr>
                                        <td>{upgrade.file}</td>
                                        <td>{upgrade.type}</td>
                                        <td>{upgrade.identifier}</td>
                                        <td>{upgrade.currentVersion}</td>
                                        <td>{upgrade.availableVersion}</td>
                                    </tr>))}
                                    </tbody>
                                </table>

                                <p className="uk-margin">
                                    {<button className="uk-button uk-button default uk-button-danger uk-margin-right"
                                             onClick={() => closeMergeRequest(selectedUpgrade.project.id)}>Mark as done/closed</button>}
                                    {!!selectedUpgrade.project.mrUrl &&
                                    <a href={selectedUpgrade.project.mrUrl} target="_blank" rel="noopener noreferrer" className="uk-button uk-button-default uk-button-primary">Go
                                        to merge request</a>}
                                </p>
                            </div>}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
}


export default UpgradePageContainer;

