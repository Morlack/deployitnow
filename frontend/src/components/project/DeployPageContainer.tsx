import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router-dom";
import {parse} from "query-string"
import DeployPage from "./DeployPage";
import {useSelector} from "react-redux";
import {projectsSelector} from "../../redux/slices/ProjectsSlice";


function DeployPageContainer() {
    const history = useHistory()
    const {id}: { id: string } = useParams()
    const [features, setFeatures] = useState(undefined)
    const [pipelineUrl, setPipelineUrl] = useState(undefined)
    const parsedParams = parse(history.location.search);
    const project = useSelector(projectsSelector).find(p => p.id === parseInt(id, 10))

    useEffect(() => {
        fetch("/api/projects/" + id + "/conflicts?environment=" + parsedParams.env + "&version=" + parsedParams.version).then(async (response) => {
            setFeatures(await response.json())
        })

        fetch("/api/projects/" + id + "/pipeline/" + parsedParams.version).then(async (response) => {
            const url = await response.text()
            setPipelineUrl(url as any)
        })
    }, [history, id, parsedParams.env, parsedParams.version])


    return <DeployPage env={parsedParams.env as string} version={parsedParams.version as string} features={features} pipelineUrl={pipelineUrl} project={project}/>
}


export default DeployPageContainer;

