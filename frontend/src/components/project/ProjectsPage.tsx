import React from "react";
import {Project, ProjectFilters} from "../../redux/slices/ProjectsSlice";
import {Link} from "react-router-dom";
import ProjectDetail from "./detail/ProjectDetail";

interface Props {
    projects: Project[],
    filters: ProjectFilters,
    selectedProject: Project | null
    onShowInactiveChange: (value: boolean) => void,
    onTeamChange: (value: number) => void,
    onSearchChange: (value: string) => void,
}


function ProjectsPage({projects, filters, onShowInactiveChange, onTeamChange, onSearchChange, selectedProject}: Props) {


    return <div className="uk-padding uk-container ">
        <div className="uk-flex">
            <div>
                <div className="uk-card uk-card-default uk-card-body">
                    <div className="uk-margin-bottom">
                        <input className="uk-input" type="text" placeholder="Search for a project" value={filters.search || ""}
                               onChange={(v) => onSearchChange(v.target.value || "")}/>
                    </div>
                    <div className="uk-margin uk-child-width-auto">
                        <label><input className="uk-checkbox" type="checkbox" checked={filters.showInactive}
                                      onChange={() => onShowInactiveChange(!filters.showInactive)}/> Show projects without environments</label>
                    </div>
                    {/*<div className="uk-margin uk-child-width-auto">*/}
                    {/*    <select className="uk-select" value={filters.team} onChange={e => this.setState({teamsFilter: e.target.value})}>*/}
                    {/*        <option value="">Filter by team...</option>*/}
                    {/*        <option value="NO_TEAM">Has no team assigned</option>*/}
                    {/*        {this.props.teams.map(t => <option key={t.id} value={t.id}>{t.name}</option>)}*/}
                    {/*    </select>*/}
                    {/*</div>*/}
                    <ul className="uk-nav uk-nav-default">
                        {projects.map(project => <li key={project.id}>
                                <Link to={"/projects/" + project.id}>{project.name}</Link>
                            </li>)}
                    </ul>
                </div>
            </div>
            <div className="uk-flex-1">
                <div className="uk-container uk-margin-left">
                    {selectedProject && <ProjectDetail project={selectedProject}/>}
                </div>
            </div>
        </div>
    </div>
}


export default ProjectsPage;

