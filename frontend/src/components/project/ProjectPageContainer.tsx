import React, {useEffect} from "react";
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {
    filteredProjectsSelector,
    projectsFiltersSelector,
    projectsLoadingSelector,
    searchUpdated,
    selectedProjectSelector,
    selectProject,
    showInactiveUpdated,
    teamUpdated
} from "../../redux/slices/ProjectsSlice";
import ProjectsPage from "./ProjectsPage";
import Loading from "../common/Loading";


function ProjectPageContainer() {
    const {id}: { id: string } = useParams()
    const dispatch = useDispatch();
    const projects = useSelector(filteredProjectsSelector)
    const filters = useSelector(projectsFiltersSelector)
    const loading = useSelector(projectsLoadingSelector)
    const selectedProject = useSelector(selectedProjectSelector)

    useEffect(() => {
        const parsedId = id ? parseInt(id, 10) : null;
        if (parsedId !== selectedProject?.id ?? null) {
            dispatch(selectProject(parseInt(id, 10)))
        }
    }, [id, selectedProject, dispatch])

    if (loading) {
        return <Loading/>
    }

    return <ProjectsPage filters={filters}
                         selectedProject={selectedProject}
                         projects={projects}
                         onSearchChange={(val) => dispatch(searchUpdated(val))}
                         onShowInactiveChange={(val) => dispatch(showInactiveUpdated(val))}
                         onTeamChange={(val) => dispatch(teamUpdated(val))}/>
}


export default ProjectPageContainer;

