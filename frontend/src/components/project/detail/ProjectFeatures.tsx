import {Project} from "../../../redux/slices/ProjectsSlice";
import React, {useEffect, useState} from "react";
import Loading from "../../common/Loading";
import {Link} from "react-router-dom";
import moment from "moment";

export function ProjectFeatures({project}: { project: Project }) {
    const [features, setFeatures] = useState([] as any[])

    useEffect(() => {
        fetch("/api/projects/" + project.id + "/features").then(async (response) => {
            setFeatures(await response.json())
        })
    }, [project])
    if (features == null) {
        return <Loading/>
    }

    return <table className="uk-table uk-table-divider uk-table-responsive">
        <thead>
        <tr>
            <th>Issue</th>
            <th>Versions</th>
            <th>Name</th>
            <th>Last updated</th>
            <th>Archived on</th>
        </tr>
        </thead>
        <tbody>
        {features.map(feature =>
            <tr key={feature.featureId}>
                <td>{feature.issue}</td>
                <td>{feature.versions.map((version: string) => <div key={version}>{version}</div>)}</td>
                <td><Link to={"/feature/" + feature.featureId}>{feature.name}</Link></td>
                <td>{moment(feature.lastUpdated).format("DD-MM HH:mm")}</td>
                <td>{feature.archivedAt ? moment(feature.archivedAt).format("DD-MM HH:mm") : "-"}</td>
            </tr>
        )}
        </tbody>
    </table>
}
