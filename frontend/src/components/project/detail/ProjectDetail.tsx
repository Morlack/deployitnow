import React, {useState} from "react";
import {Project} from "../../../redux/slices/ProjectsSlice";
import {ProjectEnvironments} from "./ProjectEnvironments";
import {ProjectFeatures} from "./ProjectFeatures";
import {ProjectDependencies} from "./ProjectDependencies";
import {ProjectTasks} from "./ProjectTasks";
import {ProjectUpgrades} from "./ProjectUpgrades";
import {Link, Route, useRouteMatch} from "react-router-dom";
import {toast} from "react-toastify";
import {post} from "../../../util/fetch";

/* eslint jsx-a11y/anchor-is-valid: "off"*/
function ProjectDetail({project}: { project: Project }) {
    const match = useRouteMatch();
    const [synchronizing, setSynchronizing] = useState(false)
    const synchronize = () => {
        setSynchronizing(true)
        const toastId = toast("Starting synchronization...", {type: 'default'})
        post("/api/projects/"+ project.id + "/sync", null).then(() => {
            toast.dismiss(toastId)
            toast("Synchronized project!", {type: 'success'})
            setSynchronizing(false)
        }).catch(() => {
            toast("Could not sync project", {type: 'error'})
            setSynchronizing(false)
        })
    }

    return <div className="uk-card uk-card-default uk-padding-remo">
        <div className={"uk-card-body"}>
            <div className="uk-flex">
                <div className="uk-flex-1"><h3>{project.name}</h3></div>
                <div className="uk-flex">
                    <button className="uk-button uk-button-small uk-button-primary" disabled={synchronizing} onClick={synchronize}>Synchronize manually</button>
                </div>
            </div>
            <ul className="uk-tab">
                <Route path={match.url} exact>{({match: childMatch}) => <li className={childMatch ? 'uk-active' : ''}><Link to={match.url}>Current environments</Link></li>}</Route>
                <Route path={match.url + '/features'} exact>{({match: childMatch}) => <li className={childMatch ? 'uk-active' : ''}><Link to={match.url + '/features'}>Feature History</Link></li>}</Route>
                <Route path={match.url + '/dependencies'} exact>{({match: childMatch}) => <li className={childMatch ? 'uk-active' : ''}><Link to={match.url + '/dependencies'}>Dependencies</Link></li>}</Route>
                <Route path={match.url + '/tasks'} exact>{({match: childMatch}) => <li className={childMatch ? 'uk-active' : ''}><Link to={match.url + '/tasks'}>Tasks</Link></li>}</Route>
                <Route path={match.url + '/upgrades'} exact>{({match: childMatch}) => <li className={childMatch ? 'uk-active' : ''}><Link to={match.url + '/upgrades'}>Upgrades</Link></li>}</Route>
            </ul>

            <Route path={match.path} exact render={() => <ProjectEnvironments project={project}/>}/>
            <Route path={match.path + '/features'} render={() => <ProjectFeatures project={project}/>}/>
            <Route path={match.path + '/dependencies'} render={() => <ProjectDependencies project={project}/>}/>
            <Route path={match.path + '/tasks'} render={() => <ProjectTasks project={project}/>}/>
            <Route path={match.path + '/upgrades'} render={() => <ProjectUpgrades project={project}/>}/>
        </div>
    </div>
}

export default ProjectDetail;
