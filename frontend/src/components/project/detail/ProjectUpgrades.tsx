import {Project} from "../../../redux/slices/ProjectsSlice";
import React, {useCallback, useEffect, useState} from "react";
import {toast} from "react-toastify";

export function ProjectUpgrades({project}: { project: Project }) {
    const [pendingUpdates, setPendingUpdates] = useState([] as any[])

    const updatePendingUpdates = useCallback(() => {
        fetch("/api/projects/" + project.id + "/tasks/pendingupgrades").then(async (response) => {
            setPendingUpdates(await response.json())
        })
    }, [project.id]);

    const startUpgradeScan = useCallback(() => {
        fetch("/api/projects/" + project.id + "/task/scanner/start", {method: 'post'}).then(async (response) => {
            toast("Successfully started scan", {type: "success"})
        })

    }, [project.id]);

    useEffect(() => {
        updatePendingUpdates();
        const interval = setInterval(updatePendingUpdates, 2000);

        return () => {
            clearInterval(interval)
        }
    }, [project, updatePendingUpdates])

    return <div>
        <h5>Pending dependency upgrades</h5>
        <button className="uk-button uk-button-small uk-button-primary" onClick={startUpgradeScan}>Start Upgrade scan</button>
        <table className="uk-table uk-table-divider uk-table-responsive">
            <thead>
            <tr>
                <th>Technology</th>
                <th>File</th>
                <th>Dependency</th>
                <th>Current version</th>
                <th>Updated version</th>
            </tr>
            </thead>
            <tbody>
            {pendingUpdates.map(update =>
                <tr key={update.dependency}>
                    <td>{update.technology}</td>
                    <td>{update.file}</td>
                    <td>{update.dependency}</td>
                    <td>{update.oldVersion}</td>
                    <td>{update.newVersion}</td>
                </tr>
            )}
            </tbody>
        </table>
    </div>
}
