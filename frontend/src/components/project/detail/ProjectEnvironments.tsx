import {Project} from "../../../redux/slices/ProjectsSlice";
import React from "react";
import ReactMarkdown from "react-markdown";

export function ProjectEnvironments({project}: { project: Project }) {
    if (project.environments.length === 0) {
        return <div>No environments known for this project</div>
    }
    return <table className="uk-table uk-table-divider">
        <thead>
        <tr>
            <th>Environment</th>
            <th>Version</th>
            <th>Release notes</th>
        </tr>
        </thead>
        <tbody>
        {project.environments.filter(env => !env.children || env.children.length === 0).map(env =>
            <tr key={env.name}>
                <td>{env.name}</td>
                <td>{env.currentVersion?.versionNumber || "-"}</td>
                <td><ReactMarkdown>{env.currentVersion?.releaseNotes}</ReactMarkdown></td>
            </tr>
        )}
        </tbody>
    </table>
}
