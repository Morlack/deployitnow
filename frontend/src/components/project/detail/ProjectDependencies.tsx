import {Project} from "../../../redux/slices/ProjectsSlice";
import React, {useEffect, useState} from "react";
import Loading from "../../common/Loading";

export function ProjectDependencies({project}: { project: Project }) {
    const [dependencies, setDependencies] = useState(null as any)

    useEffect(() => {
        fetch("/api/projects/" + project.id + "/dependencies").then(async (response) => {
            setDependencies(await response.json())
        })
    }, [project])
    if (dependencies == null) {
        return <Loading/>
    }

    return <div>
        <div>
            <h4>Depends on</h4>
            <p>These are the other interfaces this service relies on.</p>
            <table className="uk-table uk-table-divider">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Provided By</th>
                </tr>
                </thead>
                <tbody>
                {dependencies.consumes.map((consumingDep: any) =>
                    <tr key={consumingDep.type + consumingDep.name}>
                        <td>{consumingDep.type}</td>
                        <td>{consumingDep.name}</td>
                        <td>{consumingDep.providedBy.map((provider: any) => <div key={"provider-" + provider.id}>{provider.name}</div>)}</td>
                    </tr>
                )}
                </tbody>
            </table>
        </div>
        <div>
            <h4>Provides</h4>
            <p>These are the other interfaces this service provides.</p>
            <table className="uk-table uk-table-divider">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Consumed By</th>
                </tr>
                </thead>
                <tbody>
                {dependencies.provides.map((providingDep: any) =>
                    <tr key={providingDep.type + providingDep.name}>
                        <td>{providingDep.type}</td>
                        <td>{providingDep.name}</td>
                        <td>{providingDep.consumedBy.map((provider: any) => <div key={"provider-" + provider.id}>{provider.name}</div>)}</td>
                    </tr>
                )}
                </tbody>
            </table>
        </div>
    </div>
}
