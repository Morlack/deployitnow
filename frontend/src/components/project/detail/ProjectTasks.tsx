import {Project} from "../../../redux/slices/ProjectsSlice";
import React, {useCallback, useEffect, useState} from "react";
import moment from "moment";

export function ProjectTasks({project}: { project: Project }) {
    const [runningTasks, setRunningTasks] = useState([] as any[])

    const updateTasks = useCallback(() => {
        fetch("/api/projects/" + project.id + "/tasks/running").then(async (response) => {
            setRunningTasks(await response.json())
        })
    }, [project.id]);

    const handleEndTask = (name: string) => {
        fetch("/api/projects/" + project.id + "/tasks/" + name + "/end", {method: 'post'}).then(async () => {
            updateTasks();
        })
    }

    useEffect(() => {
        updateTasks();
    }, [project, updateTasks])

    return <div>
        <h5>Running tasks</h5>
        <table className="uk-table uk-table-divider uk-table-responsive">
            <thead>
            <tr>
                <th>Name</th>
                <th>Started</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {runningTasks.map(task =>
                <tr key={task.name}>
                    <td>{task.name}</td>
                    <td>{moment(task.start).format('DD-MM HH:mm')}</td>
                    <td>
                        <button className="uk-button uk-button-small uk-button-secondary" onClick={() => handleEndTask(task.name)}>Cancel manually</button>
                    </td>
                </tr>
            )}
            </tbody>
        </table>
    </div>
}
