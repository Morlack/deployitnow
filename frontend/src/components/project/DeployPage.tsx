import React from "react";
import {FeatureSummary} from "../../redux/slices/FeaturesSlice";
import Loading from "../common/Loading";
import {Project} from "../../redux/slices/ProjectsSlice";


function DeployPage({env, features, pipelineUrl, version, project}: { env: string, features?: FeatureSummary[], pipelineUrl?: string, version: string, project?: Project }) {
    if(!features || !pipelineUrl) {
        return <Loading/>
    }

    return <div className="uk-padding uk-container ">
        <div className="uk-card uk-card-default">
            <div className="uk-card-body">
                <p>You are about to deploy the following features of project <b>{project?.name}</b> to <b>{env}</b> by deploying version: {version}.</p>

                {features.length > 1 && <div data-uk-alert className="uk-alert-danger">You are about to deploy multiple features at once, and might be deploying more features than intended. Please review carefully.</div>}

                <table className="uk-table uk-table-divider uk-table-justify">
                    <thead>
                    <tr>
                        <th>Issue</th>
                        <th>Feature name</th>
                        <th>Introduced in version(s)</th>
                    </tr>
                    </thead>
                    <tbody>
                    {features.map(f => <tr key={f.featureId}>
                        <td>{f.issue || '-'}</td>
                        <td>{f.name}</td>
                        <td>{f.versions.map(v => <div>{v}</div>)}</td>
                    </tr>)}
                    </tbody>
                </table>

                <div>
                    <button className="uk-button uk-button-default uk-float-left" onClick={() => window.history.back()}>Back</button>
                    <a className="uk-button uk-button-primary uk-float-right" href={pipelineUrl} target="_blank" rel="nofollow noopener noreferrer">Go to pipeline</a>
                </div>
            </div>
        </div>
    </div>
}


export default DeployPage;

