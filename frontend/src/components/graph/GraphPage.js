import React from "react";
import * as _ from 'lodash';
import {Network, Node, Edge} from 'react-vis-network';

export class GraphPage extends React.Component {
    state = {
        edges: null
    };


    componentDidMount() {
        fetch("/api/dependencies/overview").then(async (response) => {
            this.setState({
                edges: await response.json()
            });
        })
    }


    render() {
        if (!this.state.edges) {
            return (<div className={"page-container"}>Loading....</div>)
        }

        const nodes = _.uniq(this.state.edges.map(e => e.origin).concat(this.state.edges.map(e => e.destination)))

        const edges = Object.values(_.groupBy(this.state.edges, e => e.origin + e.destination + e.type)).map(e => {
            return {
                id: e[0].origin + e[0].destination + e[0].type,
                to: e[0].origin,
                from: e[0].destination,
                width: e.length,
            }
        })

        return <div className="uk-flex">
            <Network style={{height: "80vh"}} options={{

            }}>
                {nodes.map(n => <Node key={n} id={n} label={n}/>)}
                {edges.map(n => <Edge key={n.id} {...n}/>)}
            </Network>
        </div>
    }
}
