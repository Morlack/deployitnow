import React from "react";
import {Feature, FeatureProjectEnvironment} from "../../redux/slices/FeaturesSlice";
import * as _ from "lodash";
import {Link} from "react-router-dom";
import moment from "moment";
import {post} from "../../util/fetch";
import {toast} from "react-toastify";

/* eslint "no-restricted-globals": "off" */
function FeatureComponent({feature}: { feature: Feature }) {
    const environments = _.uniq(feature.projects.flatMap(f => f.environments.map(e => e.name)))
    const archive = () => {
        if (confirm("Are you sure you want to archive this feature?")) {
            post("/api/features/" + feature.featureId + "/archive", null).then(() => {
                toast("Successfully archived feature", {type: "success"})
            }).catch(() => {
                toast("Failed to archive feature", {type: "error"})
            })
        }
    }

    return <div className='uk-card uk-card-default uk-margin'>
        <div className="uk-card-header">
            <h2>{feature.issue && <a href={"https://jira.hamis.nl/browse/" + feature.issue} target="_blank"
                                     rel="noopener noreferrer">[{feature.issue}]</a>} {feature.name}</h2>
        </div>
        <div className="uk-card-body">
            {feature.archivedAt && <div data-uk-alert className="uk-alert-warning">
                This feature has been archived on {moment(feature.archivedAt).format('DD-MM HH:mm')}.
            </div>}
            <table className='uk-table uk-table-divider uk-table-responsive uk-width-1-1'>
                <thead>
                <tr>
                    <th>Service</th>
                    <th>Version</th>
                    {environments.map(env =>
                        <th key={env} className="uk-text-center">{env}</th>
                    )}
                </tr>
                </thead>
                <tbody>
                {feature.projects.map(p =>
                    <tr key={p.projectId}>
                        <td>
                            <Link className="uk-button uk-button-primary uk-button-small" to={"/projects/" + p.projectId}
                                  style={{width: "200px"}}>{p.projectName}</Link>
                        </td>
                        <td>{p.versionNumber}</td>
                        {environments.map(env =>
                            <td key={"" + p.projectName + env}>
                                <FeatureProjectEnvironmentComponent key={env}
                                                                    environment={p.environments.find(e => e.name === env)}
                                                                    projectId={p.projectId}
                                                                    version={p.versionNumber}/>
                            </td>
                        )}
                    </tr>
                )}
                </tbody>
            </table>
        </div>
        <div className="uk-card-footer">
            <button onClick={archive} className="uk-button uk-button-small uk-button-text">{feature.archivedAt == null ? 'Archive' : 'Unarchive'} this feature</button>
        </div>
    </div>
}

function FeatureProjectEnvironmentComponent({environment, projectId, version}: { environment?: FeatureProjectEnvironment, projectId: number, version: string }) {
    if (!environment) {
        return <></>
    }
    if (environment.children && environment.children.length > 0) {
        return <div>{environment.children.map(c => FeatureProjectEnvironmentComponent({environment: c, projectId, version}))}</div>;
    }
    if (environment.deployed) {
        return <div key={environment.name} className="uk-align-center uk-text-center" title={environment.name}>
            <span className={"fa fa-check uk-text-success"}/>
        </div>
    }

    return <div key={environment.name} className="uk-align-center uk-text-center" title={environment.name}>{
        <Link to={"/project/" + projectId + "/deploy?env=" + environment.name + "&version=" + version} className="uk-button uk-button-small uk-button-default"><i
            className={"fa fa-play-circle-o"}/></Link>}
    </div>
}

export default FeatureComponent;
