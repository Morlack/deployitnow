import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import { useHistory } from "react-router-dom";
import {featureLoadingSelector, featureSearchSelector, filteredFeaturesSelector, searchUpdated} from "../../redux/slices/FeaturesSlice";
import OverviewPage from "./OverviewPage";
import {parse} from "query-string"


function OverviewPageContainer() {
    const dispatch = useDispatch();
    const items = useSelector(filteredFeaturesSelector)
    const search = useSelector(featureSearchSelector)
    const loading = useSelector(featureLoadingSelector)
    const history = useHistory()

    const handleSearchValue = (value: string) => {
        dispatch(searchUpdated(value.toLowerCase()))
        history.replace({
            pathname: '/overview',
            search: '?search=' + value.toLowerCase()
        })
    }

    useEffect(() => {
        const parsedParams = parse(history.location.search);
        if(parsedParams.search && parsedParams.search !== search) {
            handleSearchValue(parsedParams.search as string)
        }
    })

    return <OverviewPage loading={loading} features={items} search={search} searchUpdated={handleSearchValue}/>
}


export default OverviewPageContainer
