import React from "react";
import {Feature} from "../../redux/slices/FeaturesSlice";
import FeatureComponent from "../feature/Feature";
import Loading from "../common/Loading";
import {Link} from "react-router-dom";

interface Props {
    features: Feature[],
    search: string,
    searchUpdated: (term: string) => void,
    loading: boolean,
}

function OverviewPage({features, search, searchUpdated, loading}: Props) {
    return <div className={"uk-container uk-flex"}>
        <div className="uk-flex-1 uk-padding">

            <div className='uk-card uk-card-default uk-card-body'>
                <h3>Search</h3>
                <form className="uk-search uk-search-default">
                    <span data-uk-search-icon></span>
                    <input type="search" placeholder="Filter features by issue, name or project" value={search} onChange={(v) => searchUpdated(v.target.value)}
                           className="uk-search-input"/>
                </form>
                <div>
                    <button className="uk-button uk-button-small uk-button-secondary uk-margin-top" type="button" onClick={() => searchUpdated('')}>Clear search</button>
                </div>
            </div>

            <div className='uk-card uk-card-default uk-card-body uk-margin-large-top'>
                <h3>Guide</h3>
                <p>Don't understand something? Check out the guide</p>
                <Link className="uk-button uk-button-small uk-button-primary" to="/guide">Go to guide</Link>
            </div>
        </div>
        <div className="uk-flex-1 uk-padding">
            {loading && <Loading/>}
            {features.map(f => {
                return <FeatureComponent feature={f}/>
            })}
        </div>
    </div>
}

export default OverviewPage
