import React from 'react';
import 'uikit/dist/css/uikit.min.css'
import 'uikit/dist/js/uikit.min.js'
import 'uikit/dist/js/uikit-icons.min.js'
import "font-awesome/css/font-awesome.css";
import './App.css';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./redux/store";
import Subscribers from "./subscribers/Subscribers";
import Layout from "./layout/Layout";
import {ToastContainer} from "react-toastify";

import 'react-toastify/dist/ReactToastify.css';


function App() {
    return (

        <Provider store={store}>
            <ToastContainer />
            <BrowserRouter>
                <Layout/>
            </BrowserRouter>

            <Subscribers/>
        </Provider>
    );
}

export default App;
