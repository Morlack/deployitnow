import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {loggedInSelector} from "../redux/slices/UserSlice";
import {fetchSettings} from "../redux/slices/SettingSlice";


export default function SettingsSliceSubscriber() {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector(loggedInSelector);

    useEffect(() => {
        if(isLoggedIn) {
            dispatch(fetchSettings());
        }
    }, [dispatch, isLoggedIn])
    return <></>
}
