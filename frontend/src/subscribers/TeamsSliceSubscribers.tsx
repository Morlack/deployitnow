import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {registerListener} from "../redux/websockets";
import {loggedInSelector} from "../redux/slices/UserSlice";
import {fetchTeams} from "../redux/slices/TeamsSlice";


function TeamsSliceSubscribers() {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector(loggedInSelector);

    useEffect(() => {
        if (!isLoggedIn) {
            return;
        }
        const subscriptions = [
            registerListener("/topic/teams", () => dispatch(fetchTeams()))
        ];

        // Initial load
        dispatch(fetchTeams());

        return () => {
            subscriptions.forEach(cleanup => cleanup())
        }
    }, [dispatch, isLoggedIn])
    return <></>
}

export default TeamsSliceSubscribers;
