import React from "react";
import FeatureSliceSubscribers from "./FeatureSliceSubscribers";
import ProjectSliceSubscribers from "./ProjectSliceSubscribers";
import UserSliceSubscribers from "./UserSliceSubscribers";
import SettingsSliceSubscriber from "./SettingsSliceSubscribers";
import TeamsSliceSubscribers from "./TeamsSliceSubscribers";


function Subscribers() {
    return <>
        <FeatureSliceSubscribers/>
        <ProjectSliceSubscribers/>
        <UserSliceSubscribers/>
        <SettingsSliceSubscriber/>
        <TeamsSliceSubscribers/>
    </>
}

export default Subscribers;
