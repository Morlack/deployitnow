import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {fetchUser} from "../redux/slices/UserSlice";


function UserSliceSubscribers() {
    const dispatch = useDispatch();

    useEffect(() => {
        const userFetchInterval = setInterval(() => {
            dispatch(fetchUser());
        }, 2000)
        dispatch(fetchUser());

        return () => {
            clearInterval(userFetchInterval)
        }
    }, [dispatch])
    return <></>
}

export default UserSliceSubscribers;
