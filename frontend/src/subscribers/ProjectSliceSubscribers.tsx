import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {registerListener} from "../redux/websockets";
import {fetchProjects} from "../redux/slices/ProjectsSlice";
import {loggedInSelector} from "../redux/slices/UserSlice";


function ProjectSliceSubscribers() {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector(loggedInSelector);

    useEffect(() => {
        if (!isLoggedIn) {
            return;
        }
        const subscriptions = [
            registerListener("/topic/projects", () => dispatch(fetchProjects()))
        ];

        // Initial load
        dispatch(fetchProjects());

        return () => {
            subscriptions.forEach(cleanup => cleanup())
        }
    }, [dispatch, isLoggedIn])
    return <></>
}

export default ProjectSliceSubscribers;
