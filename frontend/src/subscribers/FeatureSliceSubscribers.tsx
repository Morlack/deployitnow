import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {registerListener} from "../redux/websockets";
import {fetchFeatures} from "../redux/slices/FeaturesSlice";
import {loggedInSelector} from "../redux/slices/UserSlice";


function FeatureSliceSubscribers() {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector(loggedInSelector);

    useEffect(() => {
        if (!isLoggedIn) {
            return;
        }
        const subscriptions = [
            registerListener("/topic/features", () => dispatch(fetchFeatures()))
        ];
        // Initial load
        dispatch(fetchFeatures())

        return () => {
            subscriptions.forEach(cleanup => cleanup())
        }
    }, [dispatch, isLoggedIn])
    return <></>
}

export default FeatureSliceSubscribers;
