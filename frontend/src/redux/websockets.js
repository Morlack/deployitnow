import Stomp from 'stompjs'

let url = (window.location.href.startsWith('https') ? 'wss' : 'ws') + '://' + window.location.host + '/api/ws'
if(window.location.host.indexOf('localhost') > -1) {
    url = 'ws://localhost:8080/api/ws'
}
const client = Stomp.client(url);
const topicMap = {};

let connected = false;

function subscribeToTopic(topic) {
    client.subscribe(topic, (frame) => {
        topicMap[topic].forEach(cb => cb(frame))
    })
}

function connect() {
    client.connect({}, function () {
        console.log('Connected to websockets: ');
        Object.keys(topicMap).forEach(topic => {
            subscribeToTopic(topic)
        });
        connected = true;
    }, function () {
        console.log("disconnected!")
        connected = false;
    });
}


setInterval(() => {
    const loggedIn = true//store.getState()?.loginInformation;
    if(!connected && loggedIn) {
        connect();
    } else if(connected && !loggedIn) {
        client.disconnect()
    }
}, 2000)
connect();


export const isConnected = () => {
    return connected;
}

export const registerListener = (topic, callback) => {
    if(!topicMap[topic]) {
        topicMap[topic] = [];
        if(connected) {
            subscribeToTopic(topic)
        }
    }

    topicMap[topic].push(callback);
    return () => {
        topicMap[topic].splice(topicMap[topic].indexOf(callback), 1);
    }
}
