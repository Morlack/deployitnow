import {createAsyncThunk, createSelector, createSlice, PayloadAction} from '@reduxjs/toolkit'

export interface Feature {
    featureId: number,
    name: string,
    issue?: string,
    lastUpdated: string,
    archivedAt?: string,
    projects: FeatureProject[],
}

export interface FeatureSummary {
    featureId: number,
    name: string,
    issue?: string,
    lastUpdated: string,
    archivedAt?: string,
    versions: string[]
}

export interface FeatureProject {
    projectId: number,
    projectName: string,
    versionNumber: string,
    environments: FeatureProjectEnvironment[],
    releaseDate: string,
}

export interface FeatureProjectEnvironment {
    name: string,
    deployed: boolean,
    children: FeatureProjectEnvironment[]
}

interface FeatureState {
    loading: boolean,
    error: boolean,
    items: Feature[],
    search: string,
}

export const fetchFeatures = createAsyncThunk(
    'features/retrieve',
    async () => {
        const response = await fetch('/api/features', {method: 'GET'})
        return response.json()
    }
)

const featuresSlice = createSlice({
    name: '@features',
    initialState: {
        loading: true,
        error: false,
        items: [],
        search: '',
    } as FeatureState,

    reducers: {
        searchUpdated: (state, action: PayloadAction<string>) => {
            state.search = action.payload;
        }
    },

    extraReducers: {
        [fetchFeatures.fulfilled as unknown as string]: (state, action: PayloadAction<Feature[]>) => {
            state.items = action.payload
            state.loading = false
        }
    }
});


export const featureSliceSelector = ({features}: { features: FeatureState }) => features;
export const featuresSelector = createSelector(featureSliceSelector, (featureSlice: FeatureState) => featureSlice.items);
export const featureSearchSelector = createSelector(featureSliceSelector, (featureSlice: FeatureState) => featureSlice.search);
export const featureLoadingSelector = createSelector(featureSliceSelector, (featureSlice: FeatureState) => featureSlice.loading);

export const filteredFeaturesSelector = createSelector([featuresSelector, featureSearchSelector], (features, search) => features.filter(feature =>
    (feature.name && feature.name.toLowerCase().indexOf(search) !== -1)
    || (feature.issue && feature.issue.toLowerCase().indexOf(search) !== -1)
    || (feature.projects.find(p => p.projectName.toLowerCase().indexOf(search) !== -1)))
)

export const {searchUpdated} = featuresSlice.actions

export default featuresSlice.reducer
