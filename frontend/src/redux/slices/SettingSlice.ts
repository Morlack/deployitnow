import {createAsyncThunk, createSelector, createSlice, PayloadAction} from '@reduxjs/toolkit'

interface SettingsState {
    settings: { [name: string]: string };
    version?: string,
}

export const fetchSettings = createAsyncThunk(
    'settings/fetch',
    async () => {
        let response = await fetch('/api/settings', {method: 'GET'});
        if (response.ok) {
            return await response.json()
        }
        return null;
    }
)

const settingsSlice = createSlice({
    name: '@projects',
    initialState: {
        settings: {},
    } as SettingsState,

    reducers: {},

    extraReducers: {
        [fetchSettings.fulfilled as unknown as string]: (state, action: PayloadAction<any>) => {
            state.settings = action.payload.settingMap;
            state.version = action.payload.version
        }
    }
});

export const settingsSliceSelector = ({settings}: { settings: SettingsState }) => settings;
export const settingsSelector = createSelector(settingsSliceSelector, (settings: SettingsState) => settings.settings);

export default settingsSlice.reducer
