import {createAsyncThunk, createSelector, createSlice, PayloadAction} from '@reduxjs/toolkit'

export interface Team {
    id: number,
    name: string,
    slackChannel: string,
    depedencyCheckInterval: number,
    notificationType: string
}

export interface TeamsState {
    teams: Team[];
}

export const fetchTeams = createAsyncThunk(
    'teams/retrieve',
    async () => {
        const response = await fetch('/api/teams', {method: 'GET'})
        return response.json()
    }
)

const teamsSlice = createSlice({
    name: '@teams',
    initialState: {
        teams: []
    } as TeamsState,

    reducers: {
    },

    extraReducers: {
        [fetchTeams.fulfilled as unknown as string]: (state, action: PayloadAction<Team[]>) => {
            state.teams = action.payload
        }
    }
});


export const teamsSliceSelector = ({teams}: { teams: TeamsState }) => teams;
export const teamsSelector = createSelector(teamsSliceSelector, (teams: TeamsState) => teams.teams);

export default teamsSlice.reducer
