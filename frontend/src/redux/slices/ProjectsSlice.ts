import {createAsyncThunk, createSelector, createSlice, PayloadAction} from '@reduxjs/toolkit'

export interface Project {
    id: number,
    name: string,
    lastChecked: string,
    environments: any[]
}

export interface ProjectFilters {
    search: string | null,
    team: number | null,
    showInactive: boolean,
}

interface ProjectsState {
    loading: boolean,
    items: Project[],
    filters: ProjectFilters,
    selectedProjectId: number | null,
}

export const fetchProjects = createAsyncThunk(
    'projects/retrieve/all',
    async () => {
        let response = await fetch('/api/projects', {method: 'GET'});
        if(response.ok) {
            return response.json()
        }
        throw Error("Could not retrieve projects: " + response.statusText)
    }
)

const projectsSlice = createSlice({
    name: '@projects',
    initialState: {
        loading: true,
        items: [],
        selectedProjectId: null,
        filters: {
            search: '',
            team: null,
            showInactive: false,
        },
    } as ProjectsState,

    reducers: {
        searchUpdated: (state: ProjectsState, action: PayloadAction<string | null>) => {
            state.filters.search = action.payload && action.payload.length > 0 ? action.payload : null;
        },
        showInactiveUpdated: (state: ProjectsState, action: PayloadAction<boolean>) => {
            state.filters.showInactive = action.payload
        },
        teamUpdated: (state: ProjectsState, action: PayloadAction<number | null>) => {
            state.filters.team = action.payload;
        },
        selectProject: (state: ProjectsState, action: PayloadAction<number | null>) => {
            state.selectedProjectId = action.payload;
        }
    },

    extraReducers: {
        [fetchProjects.fulfilled as unknown as string]: (state, action: PayloadAction<Project[]>) => {
            state.items = action.payload
            state.loading = false
        }
    }
});


export const projectsSliceSelector = ({projects}: { projects: ProjectsState }) => projects;
export const projectsSelector = createSelector(projectsSliceSelector, (featureSlice: ProjectsState) => featureSlice.items);
export const projectsFiltersSelector = createSelector(projectsSliceSelector, (featureSlice: ProjectsState) => featureSlice.filters);
export const projectsLoadingSelector = createSelector(projectsSliceSelector, (featureSlice: ProjectsState) => featureSlice.loading);

export const filteredProjectsSelector = createSelector([projectsSelector, projectsFiltersSelector], (projects: Project[], filters: ProjectFilters) => {
    if(!projects) {
        return [];
    }
    console.log(projects);
    return projects.filter(p => filters.showInactive || p.environments.length > 0)
        .filter(p => filters.search == null || p.name.toLowerCase().indexOf(filters.search.toLowerCase()) !== -1)
        .sort((a, b) => a.name.localeCompare(b.name))
})


export const selectedProjectSelector = ({projects}: { projects: ProjectsState }) => {
    if (!projects.selectedProjectId) {
        return null;
    }
    return projects.items.find(p => p.id === projects.selectedProjectId) ?? null
}


export const {
    searchUpdated,
    teamUpdated,
    showInactiveUpdated,
    selectProject
} = projectsSlice.actions

export default projectsSlice.reducer
