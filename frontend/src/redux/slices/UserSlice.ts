import {createAsyncThunk, createSelector, createSlice, PayloadAction} from '@reduxjs/toolkit'

interface UserState {
    loggedIn: boolean;
    user: string;
}

export const fetchUser = createAsyncThunk(
    'user/whoami',
    async () => {
        let response = await fetch('/api/auth/whoami', {method: 'GET'});
        if(response.ok) {
            return await response.text()
        }
        throw Error("Not logged in")
    }
)

const projectsSlice = createSlice({
    name: '@projects',
    initialState: {
        loggedIn: false
    } as UserState,

    reducers: {

    },

    extraReducers: {
        [fetchUser.fulfilled as unknown as string]: (state, action: PayloadAction<string>) => {
            state.loggedIn = true
            state.user = action.payload
        },

        [fetchUser.rejected as unknown as string]: (state) => {
            state.loggedIn = false
        }
    }
});


export const userSliceSelector = ({user}: { user: UserState }) => user;
export const loggedInSelector = createSelector(userSliceSelector, (user: UserState) => user.loggedIn);
export const usernameSelector = createSelector(userSliceSelector, (user: UserState) => user.user);

export default projectsSlice.reducer
