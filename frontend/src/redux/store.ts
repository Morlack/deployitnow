import {combineReducers, configureStore} from "@reduxjs/toolkit";
import featureSliceReducers from "./slices/FeaturesSlice";
import projectsSliceReducers from "./slices/ProjectsSlice";
import userSliceReducers from "./slices/UserSlice";
import settingsSlice from "./slices/SettingSlice";
import teamsSlice from "./slices/TeamsSlice";

const store = configureStore({
    reducer: combineReducers({
        features: featureSliceReducers,
        projects: projectsSliceReducers,
        user: userSliceReducers,
        settings: settingsSlice,
        teams: teamsSlice,
    })
})

export default store;
