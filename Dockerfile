FROM openjdk:8u131
VOLUME /tmp
ENV JAVA_OPTS -Xmx1024m
ADD backend/target/deployitnow*.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE 8080
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
