package com.insidion.deployitnow.infrastructure.config

import com.insidion.deployitnow.domain.entities.Feature
import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.service.PipelineResolverService
import com.insidion.deployitnow.domain.service.TicketSystemService
import com.insidion.deployitnow.repository.FeatureRepository
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.ZonedDateTime

@Configuration
class DefaultConfiguration {
    @Bean
    @ConditionalOnMissingBean(PipelineResolverService::class)
    fun defaultPipelineResolver(): PipelineResolverService {
        return DefaultPipelineResolverService()
    }

    @Bean
    @ConditionalOnMissingBean(TicketSystemService::class)
    fun defaultTicketSystemService(featureRepository: FeatureRepository): TicketSystemService {
        return DefaultTicketSystemService(featureRepository)
    }
}


class DefaultPipelineResolverService : PipelineResolverService {
    override fun findPipelineUrlForProjectAndVersion(project: Project, version: String): String {
        throw IllegalStateException("No pipeline resolvers are currently registered")
    }
}

class DefaultTicketSystemService(
        private val featureRepository: FeatureRepository
) : TicketSystemService {
    override fun findFeatureForReleaseNote(releaseNote: String): Feature? {
        return featureRepository.findFirstByName(releaseNote).orElseGet {
            Feature(name = releaseNote, createdAt = ZonedDateTime.now())
        }
    }

    override fun ticketSystemName(): String {
        return "NONE"
    }
}
