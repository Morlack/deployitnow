package com.insidion.deployitnow.infrastructure.config

import io.micrometer.core.instrument.Timer
import org.springframework.util.StopWatch
import java.util.concurrent.TimeUnit

fun <R> Timer.withBlock(action: () -> R): R {
    val sw = StopWatch()
    sw.start()
    val result = action()
    sw.stop()
    this.record(sw.lastTaskTimeMillis, TimeUnit.MILLISECONDS)
    return result
}
