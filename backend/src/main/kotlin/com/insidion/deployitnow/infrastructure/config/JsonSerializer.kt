package com.insidion.deployitnow.infrastructure.config

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.lang.reflect.ParameterizedType
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
class JsonStringListConverter : AttributeConverter<List<String>, String> {


    companion object {
        private val objectMapper = ObjectMapper().findAndRegisterModules()
    }

    override fun convertToDatabaseColumn(p0: List<String>?): String {
        return objectMapper.writeValueAsString(p0);
    }

    override fun convertToEntityAttribute(p0: String?): List<String> {
        if(p0 == null) {
            return emptyList()
        }
        return objectMapper.readValue(p0, object : TypeReference<List<String>>() {})
    }

}


@Converter
class JsonIntListConverter : AttributeConverter<List<Int>, String> {
    companion object {
        private val objectMapper = ObjectMapper().findAndRegisterModules()
    }

    override fun convertToDatabaseColumn(p0: List<Int>?): String {
        return objectMapper.writeValueAsString(p0);
    }

    override fun convertToEntityAttribute(p0: String?): List<Int> {
        if(p0 == null) {
            return emptyList()
        }
        return objectMapper.readValue(p0, object : TypeReference<List<Int>>() {})
    }

}
