package com.insidion.deployitnow

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties
class DeployitnowApplication

fun main(args: Array<String>) {
    runApplication<DeployitnowApplication>(*args)
}
