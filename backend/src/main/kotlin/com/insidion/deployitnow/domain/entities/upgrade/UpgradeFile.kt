package com.insidion.deployitnow.domain.entities.upgrade

import javax.persistence.*

@Entity
@Table(name = "upgrade_file")
class UpgradeFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0

    @ManyToOne
    @JoinColumn(name = "upgrade_id")
    var upgrade: Upgrade

    @Column(name = "file")
    var file: String

    @Column(name = "upgraded_content")
    var upgradedContentBase64Encoded: String

    constructor(upgrade: Upgrade, file: String, upgradedContentBase64Encoded: String) {
        this.upgrade = upgrade
        this.file = file
        this.upgradedContentBase64Encoded = upgradedContentBase64Encoded
    }
}
