package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.domain.entities.Project

interface PipelineResolverService {
    fun findPipelineUrlForProjectAndVersion(project: Project, version: String): String
}
