package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.domain.entities.*
import com.insidion.deployitnow.repository.DependencyConsumerRepository
import com.insidion.deployitnow.repository.DependencyProviderRepository
import com.insidion.deployitnow.repository.DependencyRepository
import com.vdurmont.semver4j.Semver
import org.springframework.stereotype.Service

@Service
class DependencyService(
        private val dependencyRepository: DependencyRepository,
        private val dependencyConsumerRepository: DependencyConsumerRepository,
        private val dependencyProviderRepository: DependencyProviderRepository,
) {
    fun getOrCreateDependency(type: String, name: String) = dependencyRepository.findByTypeAndName(type, name) ?: dependencyRepository.save(Dependency(type, name))


    fun addConsumingDependencyToProject(project: Project, type: String, name: String) {
        val dependency = getOrCreateDependency(type, name);
        if (dependencyConsumerRepository.findByProjectAndDependency(project, dependency) != null) {
            return;
        }

        dependencyConsumerRepository.save(DependencyConsumer(project, dependency))
    }

    fun addProvidingDependencyToProject(project: Project, type: String, name: String) {
        val dependency = getOrCreateDependency(type, name);
        if (dependencyProviderRepository.findByProjectAndDependency(project, dependency) != null) {
            return;
        }

        dependencyProviderRepository.save(DependencyProvider(project, dependency))
    }

    fun getProjectConsumingDependencies(project: Project): List<DependencyConsumer> {
        return dependencyConsumerRepository.findAllByProject(project);
    }

    fun getProjectProvidingDependencies(project: Project): List<DependencyProvider> {
        return dependencyProviderRepository.findAllByProject(project);
    }

    fun deleteDependency(it: DependencyConsumer) {
        dependencyConsumerRepository.delete(it)
    }

    fun deleteDependency(it: DependencyProvider) {
        dependencyProviderRepository.delete(it)
    }

    fun getAll(): List<Dependency> = dependencyRepository.findAll();
}
