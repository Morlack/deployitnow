package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.domain.entities.Feature

interface TicketSystemService {
    fun findFeatureForReleaseNote(releaseNote: String): Feature?

    fun ticketSystemName(): String
}
