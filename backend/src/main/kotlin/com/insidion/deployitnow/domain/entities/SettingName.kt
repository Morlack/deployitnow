package com.insidion.deployitnow.domain.entities

enum class SettingName(val settingType: SettingType, val default: String? = null) {
    GENERAL_BASE_URL(SettingType.STRING),
    GENERAL_ARCHIVE_ENVIRONMENTS(SettingType.LIST_OF_STRINGS, "prod"),

    SLACK_WEBHOOK_URL(SettingType.STRING),


    GITLAB_ENABLED(SettingType.BOOL),
    GITLAB_ENABLE_PROJECT_SCAN(SettingType.BOOL),
    GITLAB_ENABLE_DETECTOR(SettingType.BOOL),
    GITLAB_ENABLE_UPGRADER(SettingType.BOOL),
    GITLAB_URL(SettingType.STRING),
    GITLAB_API_TOKEN(SettingType.STRING),
    GITLAB_ENVIRONMENTS_WHITELIST(SettingType.LIST_OF_STRINGS, "test,accp,prod"),
}

enum class SettingType {
    STRING,
    NUMBER,
    BOOL,
    LIST_OF_STRINGS,
}
