package com.insidion.deployitnow.domain.entities

import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "project_version")
class ProjectVersion {
    @Id
    @GeneratedValue(GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Column(name = "number")
    var number: String

    @Column(name = "release_notes")
    var releaseNotes: String? = null

    @Column(name = "release_date")
    var releaseDate: ZonedDateTime

    @ManyToOne()
    @JoinColumn("project_id")
    var project: Project

    @ManyToMany
    @JoinTable(
            name = "feature_project_versions",
            joinColumns = [JoinColumn(name = "version_id")],
            inverseJoinColumns = [JoinColumn(name= "feature_id")]
    )
    var features: List<Feature> = emptyList()

    constructor(number: String, project: Project, releaseDate: ZonedDateTime) {
        this.number = number
        this.project = project
        this.releaseDate = releaseDate;
    }
}
