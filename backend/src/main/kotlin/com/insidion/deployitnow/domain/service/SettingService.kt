package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.domain.entities.Setting
import com.insidion.deployitnow.domain.entities.SettingName
import com.insidion.deployitnow.domain.entities.SettingType
import com.insidion.deployitnow.repository.SettingRepository
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class SettingService(
        val settingRepository: SettingRepository
) {
    @PostConstruct
    fun ensureDefaults() {
        SettingName.values().forEach {
            val setting = settingRepository.findByName(it)
            if(setting == null) {
                settingRepository.save(Setting(it, it.default))
            }
        }
    }


    fun getAll() = settingRepository.findAll()

    fun getSettingString(settingName: SettingName): String? {
        verifySettingType(settingName, SettingType.STRING)
        return settingRepository.findByName(settingName)?.value ?: settingName.default
    }

    fun getSettingBool(settingName: SettingName): Boolean {
        verifySettingType(settingName, SettingType.BOOL)
        return settingRepository.findByName(settingName)?.value ?: settingName.default == "true"
    }

    fun getSettingNumber(settingName: SettingName): Int? {
        verifySettingType(settingName, SettingType.NUMBER)
        val stringRep = (settingRepository.findByName(settingName)?.value ?: settingName.default)
        return stringRep?.toInt()
    }

    fun getSettingListOfStrings(settingName: SettingName): List<String> {
        verifySettingType(settingName, SettingType.LIST_OF_STRINGS)
        val stringRep = (settingRepository.findByName(settingName)?.value ?: settingName.default)
        return stringRep?.split(",") ?: listOf()
    }

    fun storeValue(settingName: SettingName, value: String?) {
        val setting = settingRepository.findByName(settingName) ?: Setting(settingName, "")
        setting.value = value
        settingRepository.save(setting)
    }

    fun verifySettingType(settingName: SettingName, settingType: SettingType) {
        if(settingName.settingType != settingType) {
            throw IllegalArgumentException("Setting is not of type $settingType!")
        }
    }
}
