package com.insidion.deployitnow.domain.entities.upgrade

import javax.persistence.*

@Entity
@Table(name = "upgrade_item")
class UpgradeItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0

    @ManyToOne
    @JoinColumn(name = "upgrade_id")
    var upgrade: Upgrade

    @Column(name = "technology")
    var technology: String

    @Column(name = "file")
    var file: String

    @Column(name = "type")
    var type: String


    @Column(name = "identifier")
    var identifier: String

    @Column(name = "current_version")
    var currentVersion: String

    @Column(name = "available_version")
    var availableVersion: String

    constructor(upgrade: Upgrade, technology: String, file: String, type: String, identifier: String, currentVersion: String, availableVersion: String) {
        this.upgrade = upgrade
        this.type = type
        this.file = file
        this.identifier = identifier
        this.currentVersion = currentVersion
        this.availableVersion = availableVersion
        this.technology = availableVersion
    }
}
