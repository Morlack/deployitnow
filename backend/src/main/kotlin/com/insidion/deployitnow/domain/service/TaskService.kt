package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectRunningTask
import com.insidion.deployitnow.domain.entities.TaskResult
import com.insidion.deployitnow.repository.ProjectRunningTaskRepository
import org.slf4j.Logger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.lang.IllegalStateException
import java.time.ZonedDateTime

@Service
class TaskService(
        val logger: Logger,
        val projectRunningTaskRepository: ProjectRunningTaskRepository,
) {
    @Scheduled(fixedDelay = 60000)
    fun timeoutJobs() {
        projectRunningTaskRepository.findByFinishedNullAndStartedBefore(ZonedDateTime.now().minusMinutes(30)).forEach {
            it.result = TaskResult.TIMEOUT
            it.finished = ZonedDateTime.now()
            projectRunningTaskRepository.save(it)
            logger.info("Closed task with id {} because of timeout for project id {}", it.id, it.project.id)
        }
    }

    fun retrieveRunningTasksForProject(project: Project): List<ProjectRunningTask> {
        return projectRunningTaskRepository.findByProjectAndFinishedNull(project)
    }

    fun registerTaskForProject(project: Project, name: String) {
        logger.info("Registering task of type {} for project {}({})", name, project.id, project.name)
        if (projectRunningTaskRepository.findByProjectAndNameAndFinishedNull(project, name) != null) {
            throw IllegalStateException("Already running task of type " + name + " for project id " + project.id)
        }

        projectRunningTaskRepository.save(ProjectRunningTask(project, name, ZonedDateTime.now()))
    }

    fun registerResultForTask(project: Project, name: String, taskResult: TaskResult) {
        logger.info("Registering result {} for task of type {} for project {}({})", taskResult, name, project.id, project.name)
        val task = projectRunningTaskRepository.findByProjectAndNameAndFinishedNull(project, name) ?: throw IllegalStateException("No task exists")
        task.result = taskResult
        task.finished = ZonedDateTime.now()
        projectRunningTaskRepository.save(task)
    }
}
