package com.insidion.deployitnow.domain.tasks

import com.insidion.deployitnow.adapter.gitlab.GitlabService
import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.SettingName
import com.insidion.deployitnow.domain.entities.TaskResult
import com.insidion.deployitnow.domain.entities.upgrade.Upgrade
import com.insidion.deployitnow.domain.entities.upgrade.UpgradeFile
import com.insidion.deployitnow.domain.entities.upgrade.UpgradeItem
import com.insidion.deployitnow.domain.service.ProjectService
import com.insidion.deployitnow.domain.service.SettingService
import com.insidion.deployitnow.domain.service.TaskService
import com.insidion.deployitnow.domain.service.UpgradeService
import com.insidion.deployitnow.repository.ProjectRepository
import com.insidion.deployitnow.repository.ProjectRunningTaskRepository
import com.insidion.deployitnow.repository.ProjectTechnologyRepository
import com.insidion.deployitnow.repository.UpgradeRepository
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.nio.charset.Charset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@Service
class ScannerTask(
        val projectService: ProjectService,
        val upgradeRepository: UpgradeRepository,
        val projectRepository: ProjectRepository,
        val taskRepository: ProjectRunningTaskRepository,
        val projectTechnologyRepository: ProjectTechnologyRepository,
        val logger: Logger,
        val gitlabService: GitlabService,
        val taskService: TaskService,
        val upgradeService: UpgradeService,
        val settingService: SettingService,
) {

    @Scheduled(fixedDelay = 60000)
    fun findProjectToRunFor() {
        if (!settingService.getSettingBool(SettingName.GITLAB_ENABLE_UPGRADER)) {
            return
        }
        projectRepository.findAll().firstOrNull {
            val latestRunTime = taskRepository.findLatestByProject(it, "scanner")
            val timePassed = latestRunTime == null || latestRunTime.isBefore(ZonedDateTime.now().minusDays((it.team?.dependencyCheckInterval ?: 14).toLong()))
            if (timePassed) {
                return@firstOrNull startForProject(it)
            }
            return@firstOrNull false
        }
    }

    fun startForProject(project: Project): Boolean {
        val technologies = projectTechnologyRepository.findByProjectAndNameIn(project, listOf("maven", "npm"))
        if (technologies.isEmpty()) {
            return false
        }
        taskService.registerTaskForProject(project, "scanner")
        return try {
            val content = IOUtils.resourceToString("/gitlab/dependency-scanner/.gitlab-ci.yaml", Charset.defaultCharset())
                    .replace("%%BASE_URL%%", settingService.getSettingString(SettingName.GENERAL_BASE_URL)!!)
                    .replace("%%PROJECT_ID%%", project.id.toString())
                    .replace("%%MAVEN_ENABLED%%", if (technologies.any { it.name == "maven" }) "true" else "false")
                    .replace("%%NPM_ENABLED%%", if (technologies.any { it.name == "npm" }) "true" else "false")
            val formattedDate = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm").format(ZonedDateTime.now()).format(ZonedDateTime.now())

            gitlabService.updateFileInCommitOnNewBranchFromMaster(project.gitlabId, "micromanage/scanner/$formattedDate", ".gitlab-ci.yml", content, "Micromanage scanner run")
            true
        } catch (e: Exception) {
            taskService.registerResultForTask(project, "scanner", TaskResult.FAILURE)
            logger.warn("Could not start scanner task", e)
            false
        }
    }

    fun handleResult(projectId: Long, techName: String, dependencies: List<DependencyUpgradeCallbackFile>) {
        projectService.getProjectById(projectId).ifPresent { project ->
            val upgrade = upgradeRepository.findByProjectAndClosedFalse(project) ?: upgradeRepository.save(Upgrade(project))

            val hasChanges = (upgrade.items.isEmpty() && dependencies.isNotEmpty()) || dependencies.any {
                upgrade.files.any { f -> f.file == it.file && f.upgradedContentBase64Encoded != it.fileContentBase64 }
            }

            upgrade.items = dependencies.flatMap { file ->
                file.upgrades.map { item ->
                    UpgradeItem(upgrade, techName, file.file, item.type, item.identifier, item.currentVersion, item.availableVersion)
                }
            }
            upgrade.files = dependencies.map { UpgradeFile(upgrade, it.file, it.fileContentBase64) }
            upgrade.closed = upgrade.items.isEmpty()
            upgradeRepository.save(upgrade)

            if (hasChanges) {
                upgradeService.createOrUpdateMergeRequestForUpgrade(upgrade)
            }
        }
    }

}

data class DependencyUpgradeCallbackFile(
        var file: String,
        val fileContentBase64: String,
        var upgrades: List<DependencyUpgradeCallbackItem>
)


data class DependencyUpgradeCallbackItem(
        var type: String,
        var identifier: String,
        var currentVersion: String,
        var availableVersion: String,
)
