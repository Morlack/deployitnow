package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.adapter.gitlab.GitlabService
import com.insidion.deployitnow.adapter.slack.SlackService
import com.insidion.deployitnow.domain.entities.NotificationType
import com.insidion.deployitnow.domain.entities.upgrade.Upgrade
import com.insidion.deployitnow.repository.UpgradeRepository
import com.slack.api.model.block.Blocks.*
import com.slack.api.model.block.element.BlockElements.asElements
import com.slack.api.model.block.element.BlockElements.button
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import com.slack.api.webhook.WebhookPayloads.*
import com.slack.api.model.block.composition.BlockCompositions.*
import org.slf4j.Logger


@Service
class UpgradeService(
        private val gitlabService: GitlabService,
        private val upgradeRepository: UpgradeRepository,
        private val projectService: ProjectService,
        private val slackService: SlackService,
        private val logger: Logger,
) {

    fun createOrUpdateMergeRequestForUpgrade(upgrade: Upgrade) {
        val formattedDate = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm").format(ZonedDateTime.now())

        val existingMr = upgrade.mergeRequestId?.let {
            val mr = gitlabService.getMergeRequest(upgrade.project.gitlabId, upgrade.mergeRequestId!!)
            if(mr.state != "opened") null else mr
        }
        if(existingMr != null) {
            logger.info("Found existing MR: {}", existingMr)
        }
        val branch = existingMr?.sourceBranch ?: "micromanage/upgrader/$formattedDate"
        val upgradeMap = upgrade.files.map { file ->
            file.file to file.upgradedContentBase64Encoded
        }.toMap()

        gitlabService.updateFilesInCommitOnNewBranchFromMaster(upgrade.project.gitlabId, branch, "Dependency upgrade", upgradeMap, if (existingMr != null) null else "master")

        if (existingMr == null) {
            val mr = gitlabService.createMergeRequest(upgrade.project.gitlabId, branch, "Micromanage dependency upgrade $formattedDate")
            upgrade.mergeRequestUrl = mr.webUrl
            upgrade.mergeRequestId = mr.iid
            upgradeRepository.save(upgrade)
        }

        val team = upgrade.project.team ?: return
        val shouldSend = when (team.notificationType) {
            NotificationType.ALL -> true
            NotificationType.CREATE -> existingMr == null
            NotificationType.NEVER -> false
        }

        if (shouldSend) {
            val message = """Dependency upgrades were found for project ${upgrade.project.name}"""
            val channel = if (team.slackChannel.startsWith("#")) team.slackChannel else '#' + team.slackChannel
            slackService.sendMessage(payload { p ->
                p
                        .text("""{"channel": "$channel", "text": "$message", "username": "MicroManage "}""")
                        .channel(channel)
                        .username("MicroManage")
                        .blocks(
                                asBlocks(
                                        section { it.text(markdownText(message)) },
                                        divider(),
                                        actions { actions ->
                                            actions.elements(
                                                    asElements(
                                                            button { b -> b.text(plainText { pt -> pt.text("Go to Merge request") }).url(upgrade.mergeRequestUrl).value("go_to_mr") },
                                                    )
                                            )
                                        }))
            })
        }
    }

    fun markUpgradesWithMergeRequestAsDone(projectId: Int, mergeRequestId: Int) {
        val upgrade = upgradeRepository.findByProjectGitlabIdAndClosedFalseAndMergeRequestId(
                projectId,
                mergeRequestId
        ) ?: return
        upgrade.closed = true
        upgradeRepository.save(upgrade)
    }

    fun markUpgradesForProjectAsDone(projectId: Long) {
        val project = projectService.getProjectById(projectId).orElseThrow { throw IllegalStateException("Project does not exist!") }

        upgradeRepository.findAllByProject(project).forEach {
            it.closed = true
            upgradeRepository.save(it)
        }
    }
}
