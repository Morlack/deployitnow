package com.insidion.deployitnow.domain.entities.upgrade

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectTechnology
import javax.persistence.*

@Entity
@Table(name = "upgrade_exclusion")
class UpgradeExclusion(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        var id: Long = 0,

        @ManyToOne
        @JoinColumn(name = "project_id")
        var project: Project,

        @ManyToOne
        @JoinColumn(name = "project_technology_id")
        var projectTechnology: ProjectTechnology,

        @Column(name = "type")
        var type: String,

        @Column(name = "identifier")
        var identifier: String,

        @Column(name = "current_version")
        var currentVersion: String,

        @Column(name = "available_version")
        var availableVersion: String)
