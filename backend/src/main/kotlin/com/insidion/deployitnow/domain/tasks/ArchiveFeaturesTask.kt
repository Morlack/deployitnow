package com.insidion.deployitnow.domain.tasks

import com.insidion.deployitnow.domain.service.FeatureService
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ArchiveFeaturesTask(
        private val featureService: FeatureService
) {

    @Scheduled(fixedDelay = 5 * 1000)
    fun synchronizeProjects() {
        featureService.scanFeaturesForCompletion()
    }
}
