package com.insidion.deployitnow.domain.entities

import java.time.ZonedDateTime
import javax.persistence.*

enum class TaskType {
    DEPLOYMENT_ACCEPTANCE,
    DEPLOYMENT_PRODUCTION,
}

enum class TaskCompletionStatus {
    SUCCESS,
    FAILURE
}

@Entity
@Table(name = "task")
class Task(
        @Id
        @GeneratedValue(GenerationType.IDENTITY)
        @Column(name = "id")
        var id: Long? = null,

        @ManyToOne
        @JoinColumn("feature_id")
        val feature: Feature,

        @Column(name = "type")
        @Enumerated(EnumType.STRING)
        val type: TaskType,

        @Column(name = "requested_by")
        val requestedBy: String,

        @Column(name = "requested_remarks")
        val requestedRemarks: String? = null,

        @Column(name = "requested_at")
        val requestedAt: ZonedDateTime,


        @Column(name = "completed_by")
        var completedBy: String? = null,

        @Column(name = "completed_remarks")
        var completedRemarks: String? = null,

        @Column(name = "completed_at")
        var completedAt: ZonedDateTime? = null,

        @Column(name = "completed_status")
        @Enumerated(EnumType.STRING)
        var status: TaskCompletionStatus? = null
) {
}
