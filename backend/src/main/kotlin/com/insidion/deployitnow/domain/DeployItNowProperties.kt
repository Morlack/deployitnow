package com.insidion.deployitnow.domain

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@ConfigurationProperties(prefix = "deployitnow.config")
@Component
class DeployItNowProperties {
    lateinit var version: String
    lateinit var archiveEnvironments: List<String>
    lateinit var teams: List<String>
}

