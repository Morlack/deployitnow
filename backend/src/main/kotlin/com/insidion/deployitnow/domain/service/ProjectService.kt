package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.adapter.ws.WebsocketService
import com.insidion.deployitnow.domain.entities.Feature
import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.repository.ProjectRepository
import com.vdurmont.semver4j.Semver
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProjectService(
        private val projectRepository: ProjectRepository,
        private val featureService: FeatureService,
        private val teamService: TeamService,
        private val websocketService: WebsocketService,
) {
    fun getProjectById(id: Long): Optional<Project> {
        return projectRepository.findById(id)
    }

    fun getProjectByGitlabId(id: Int): Project? {
        return projectRepository.findByGitlabId(id)
    }

    fun getFeaturesNotOnEnvironmentBeforeVersion(project: Project, environmentName: String, versionNumber: String): List<Feature> {
        val environment = project.environments.find { it.name == environmentName }
                ?: throw IllegalArgumentException("No environment exists with name $environmentName")

        val environmentVersion = environment.currentVersion?.number

        val semver = Semver(versionNumber)
        return featureService.getAllPendingFeaturesByProject(project)
                .filter { feature ->
                    feature.projectVersions.any { pv ->
                        pv.project.id == project.id
                                && semver.isGreaterThanOrEqualTo(pv.number)
                                && (environmentVersion == null || semver.isGreaterThan(environmentVersion))
                    }
                }
    }

    fun setTeamForProject(projectId: Long, teamId: Long) {
        val project = getProjectById(projectId).orElseThrow { IllegalStateException("Project does not exist!") }
        if(teamId >= 0) {
            project.team = teamService.getById(teamId).orElseThrow { IllegalStateException("Team does not exist!") }
        } else {
            project.team = null
        }
        projectRepository.save(project)
        websocketService.sendProjectUpdated(project.id)
    }
}
