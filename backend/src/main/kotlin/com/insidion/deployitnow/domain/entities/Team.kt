package com.insidion.deployitnow.domain.entities

import javax.persistence.*

@Entity
@Table(name = "team")
class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0

    @Column(name = "name")
    var name: String

    @Column(name = "slack_channel")
    var slackChannel: String

    @Column(name = "dependeny_check_interval")
    var dependencyCheckInterval: Int

    @Column(name = "notification_type")
    @Enumerated(EnumType.STRING)
    var notificationType: NotificationType

    constructor(name: String, slackChannel: String, dependencyCheckInterval: Int, notificationType: NotificationType) {
        this.name = name
        this.slackChannel = slackChannel
        this.dependencyCheckInterval = dependencyCheckInterval
        this.notificationType = notificationType
    }
}


enum class NotificationType {
    ALL,
    CREATE,
    NEVER
}
