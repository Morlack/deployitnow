package com.insidion.deployitnow.domain.entities.upgrade

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectTechnology
import javax.persistence.*

@Entity
@Table(name = "upgrade")
class Upgrade(

        @ManyToOne
        @JoinColumn(name = "project_id")
        var project: Project,
) {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        var id: Long = 0


        @Column(name = "closed")
        var closed: Boolean = false

        @Column(name = "mr_id")
        var mergeRequestId: Int? = null

        @Column(name = "mr_url")
        var mergeRequestUrl: String? = null

        @OneToMany(mappedBy = "upgrade", cascade = arrayOf(CascadeType.ALL))
        var items: List<UpgradeItem> = emptyList()

        @OneToMany(mappedBy = "upgrade", cascade = arrayOf(CascadeType.ALL))
        var files: List<UpgradeFile> = emptyList()
}
