package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.domain.entities.ProjectVersion
import com.insidion.deployitnow.domain.events.ProjectVersionUpdatedEvent
import com.insidion.deployitnow.repository.FeatureRepository
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ReleaseNotesProcessingService(
        val featureRepository: FeatureRepository,
        val featureService: FeatureService,
        val ticketSystemServices: List<TicketSystemService>
) : ApplicationListener<ProjectVersionUpdatedEvent> {
    override fun onApplicationEvent(event: ProjectVersionUpdatedEvent) {
        processReleaseNotes(event.projectVersion)
    }

    fun processReleaseNotes(projectVersion: ProjectVersion) {
        if(projectVersion.releaseNotes == null) {
            return
        }
        projectVersion.releaseNotes!!.lines()
                .filter { it.isNotEmpty() }
                .map { it.replace("* ", "") }
                .map { it.replace("- ", "") }
                .map { it.trim() }
                .forEach {
                    val feature = ticketSystemServices.mapNotNull { tss -> tss.findFeatureForReleaseNote(it) }.first()
                    val containsVersion = feature.projectVersions.any { pv -> pv.number == projectVersion.number && pv.project.name == projectVersion.project.name }
                    if (!containsVersion) {
                        feature.projectVersions += projectVersion
                        feature.archivedAt = null
                    }

                    feature.lastUpdated = ZonedDateTime.now()
                    featureRepository.save(feature)
                    featureService.sendFeatureUpdate(feature)
                }
    }
}
