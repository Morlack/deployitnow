package com.insidion.deployitnow.domain.cron

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.infrastructure.config.withBlock
import com.insidion.deployitnow.repository.ProjectRepository
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import org.springframework.data.domain.PageRequest
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.concurrent.atomic.AtomicLong
import javax.annotation.PostConstruct

@Service
class SynchronizeProjectsCron(
        val projectSynchronizers: List<ProjectSynchronizer>,
        private val meterRegistry: MeterRegistry,
        private val projectRepository: ProjectRepository,
) {
    private val timeBetweenSyncsGauge = AtomicLong()
    private lateinit var syncTimeTimer: Timer

    @PostConstruct
    fun setup() {
        meterRegistry.gauge("deployitnow.projects.refresh.gauge", timeBetweenSyncsGauge)
        syncTimeTimer = meterRegistry.timer("deployitnow.sync.timer")
    }


    @Scheduled(fixedDelay = 1000)
    fun synchronizeProjects() {
        projectRepository
                .findByHasEnvironmentsTrueAndLastCheckedBeforeOrderByLastCheckedAsc(ZonedDateTime.now().minusMinutes(2), PageRequest.of(0, 10))
                .parallelStream()
                .forEach(this::synchronizeProject)
    }

    private fun synchronizeProject(project: Project) {
        syncTimeTimer.withBlock {
            projectSynchronizers.forEach { synchronizer ->
                synchronizer.synchronizeProject(project)
            }

            if (project.lastChecked != null) {
                timeBetweenSyncsGauge.set(ChronoUnit.SECONDS.between(project.lastChecked, ZonedDateTime.now()))
            }
            project.lastChecked = ZonedDateTime.now()
            projectRepository.save(project)
        }
    }
}
