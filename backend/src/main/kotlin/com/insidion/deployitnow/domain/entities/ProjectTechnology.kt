package com.insidion.deployitnow.domain.entities

import javax.persistence.*

@Entity
@Table(name = "project_technology")
class ProjectTechnology {
    @Id
    @GeneratedValue(GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0

    @ManyToOne
    @JoinColumn(name = "project_id")
    var project: Project

    @Column(name = "technology_name")
    var name: String


    @Column(name = "file_path")
    var filePath: String


    constructor(project: Project, technologyName: String, filePath: String) {
        this.name = technologyName
        this.filePath = filePath
        this.project = project
    }
}
