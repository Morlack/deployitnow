package com.insidion.deployitnow.domain.cron

import com.insidion.deployitnow.domain.entities.Project

interface ProjectSynchronizer {
    fun synchronizeProject(project: Project)
}
