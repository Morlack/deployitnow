package com.insidion.deployitnow.domain.events

import com.insidion.deployitnow.domain.entities.ProjectVersion
import org.springframework.context.ApplicationEvent

class ProjectVersionUpdatedEvent(source: Any, val projectVersion: ProjectVersion) : ApplicationEvent(source)
