package com.insidion.deployitnow.domain.tasks

import com.insidion.deployitnow.adapter.gitlab.GitlabService
import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectTechnology
import com.insidion.deployitnow.domain.entities.SettingName
import com.insidion.deployitnow.domain.entities.TaskResult
import com.insidion.deployitnow.domain.service.ProjectService
import com.insidion.deployitnow.domain.service.SettingService
import com.insidion.deployitnow.domain.service.TaskService
import com.insidion.deployitnow.repository.ProjectRepository
import com.insidion.deployitnow.repository.ProjectTechnologyRepository
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.springframework.data.domain.PageRequest
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.nio.charset.Charset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@Service
class DetectorTask(
        val projectService: ProjectService,
        val projectRepository: ProjectRepository,
        val projectTechnologyRepository: ProjectTechnologyRepository,
        val logger: Logger,
        val gitlabService: GitlabService,
        val taskService: TaskService,
        val settingService: SettingService,
) {

    @Scheduled(fixedDelay = 60000)
    fun findProjectToRunFor() {
        if(!settingService.getSettingBool(SettingName.GITLAB_ENABLE_DETECTOR)) {
            return
        }
        val next: List<Project?> = projectRepository.findByHasNeverHadTaskOfType("detector", PageRequest.of(0, 1))
        next.filter { it != null }.forEach { startForProject(it!!) }
    }

    fun startForProject(project: Project) {
        taskService.registerTaskForProject(project, "detector")
        try {
            val content = IOUtils.resourceToString("/gitlab/detector/.gitlab-ci.yaml", Charset.defaultCharset())
                    .replace("%%HOST%%", settingService.getSettingString(SettingName.GENERAL_BASE_URL)!!.replace(Regex("(http|https)://"), ""))
                    .replace("%%BASE_URL%%", settingService.getSettingString(SettingName.GENERAL_BASE_URL)!!)
                    .replace("%%PROJECT_ID%%", project.id.toString())
            val formattedDate = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm").format(ZonedDateTime.now()).format(ZonedDateTime.now())

            gitlabService.updateFileInCommitOnNewBranchFromMaster(project.gitlabId, "micromanage/detector/$formattedDate", ".gitlab-ci.yml", content, "Micromanage detector run")
        } catch (e: Exception) {
            taskService.registerResultForTask(project, "detector", TaskResult.FAILURE)
        }
    }

    fun handleTechnologies(projectId: Long, technologies: List<TechnologyDetectorCallbackItem>) {
        projectService.getProjectById(projectId).ifPresent {
            technologies.forEach { technology ->
                if (it.projectTechnologies.none { projectTechnology -> projectTechnology.name == technology.technology && projectTechnology.filePath == technology.file }) {
                    projectTechnologyRepository.save(ProjectTechnology(it, technology.technology, technology.file))
                }
            }
            taskService.registerResultForTask(it, "detector", TaskResult.SUCCESS)
        }
    }
}

data class TechnologyDetectorCallbackItem(
        val technology: String,
        val file: String
)
