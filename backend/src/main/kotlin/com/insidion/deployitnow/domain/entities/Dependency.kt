package com.insidion.deployitnow.domain.entities

import javax.persistence.*

@Entity
@Table(name = "dependency")
class Dependency {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0

    @Column(name = "type")
    var type: String

    @Column(name = "name")
    var name: String

    @OneToMany(mappedBy = "dependency")
    var dependencyProviders: List<DependencyProvider> = emptyList()

    @OneToMany(mappedBy = "dependency")
    var dependencyConsumers: List<DependencyConsumer> = emptyList()

    constructor(type: String, name: String) {
        this.type = type;
        this.name = name
    }

}
