package com.insidion.deployitnow.domain.entities

import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "project")
class Project {
    @Id
    @GeneratedValue(GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0

    @Column(name = "name")
    var name: String

    @Column(name = "gitlab_id")
    var gitlabId: Int

    @Column(name = "has_environments")
    var hasEnvironments: Boolean = false

    @Column(name = "last_checked")
    var lastChecked: ZonedDateTime?

    @JoinColumn(name = "team_id")
    @ManyToOne(fetch = FetchType.EAGER)
    var team: Team? = null;

    @OneToMany(mappedBy = "project", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    var environments: MutableList<ProjectEnvironment> = mutableListOf()

    @OneToMany(mappedBy = "project", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var consumedDependencies: MutableList<DependencyConsumer> = mutableListOf()

    @OneToMany(mappedBy = "project", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var providedDependencies: MutableList<DependencyProvider> = mutableListOf()

    @OneToMany(mappedBy = "project", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var projectTechnologies: MutableList<ProjectTechnology> = mutableListOf()

    @OneToMany(mappedBy = "project", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var runningTasks: MutableList<ProjectRunningTask> = mutableListOf()

    constructor(name: String, gitlabId: Int) {
        this.name = name
        this.gitlabId = gitlabId
        this.lastChecked = ZonedDateTime.ofInstant(Instant.EPOCH, ZoneId.systemDefault())
    }
}
