package com.insidion.deployitnow.domain.entities

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "feature")
class Feature(
        @Id
        @GeneratedValue(GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long = 0,

        @Column(name = "name")
        var name: String,

        @Column(name = "issue")
        var issue: String? = null,

        @ManyToMany
        @JoinTable(
                name = "feature_project_versions",
                joinColumns = [JoinColumn(name = "feature_id")],
                inverseJoinColumns = [JoinColumn(name = "version_id")]
        )
        @Fetch(FetchMode.JOIN)
        var projectVersions: List<ProjectVersion> = emptyList(),

        @OneToMany(mappedBy = "feature")
        var tasks: List<Task> = emptyList(),

        @Column(name = "updated_at")
        var lastUpdated: ZonedDateTime = ZonedDateTime.now(),

        @Column(name = "archived_at")
        var archivedAt: ZonedDateTime? = null,

        @Column(name = "created_at")
        val createdAt: ZonedDateTime
)
