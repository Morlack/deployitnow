package com.insidion.deployitnow.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import com.insidion.deployitnow.infrastructure.config.JsonIntListConverter
import com.insidion.deployitnow.infrastructure.config.JsonStringListConverter
import com.vdurmont.semver4j.Semver
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "project_running_tasks")
class ProjectRunningTask {
    @Id
    @GeneratedValue(GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @ManyToOne()
    @JoinColumn(name = "project_id")
    @JsonIgnore
    var project: Project

    @Column(name = "name")
    var name: String

    @Column(name = "dt_started")
    var started: ZonedDateTime

    @Column(name = "result")
    var result: TaskResult? = null

    @Column(name = "dt_finished")
    var finished: ZonedDateTime? = null

    constructor(project: Project, name: String, started: ZonedDateTime) {
        this.project = project
        this.name = name
        this.started = started
    }
}

enum class TaskResult {
    SUCCESS,
    FAILURE,
    CANCELLED_MANUALLY,
    TIMEOUT,
}
