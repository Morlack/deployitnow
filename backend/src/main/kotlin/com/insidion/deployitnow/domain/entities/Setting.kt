package com.insidion.deployitnow.domain.entities

import javax.persistence.*

@Entity
@Table(name = "setting")
class Setting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    var name: SettingName

    @Column(name = "value")
    var value: String?

    constructor(name: SettingName, value: String?) {
        this.name = name
        this.value = value;
    }
}
