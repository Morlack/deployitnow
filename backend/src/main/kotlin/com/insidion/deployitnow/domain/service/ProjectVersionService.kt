package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectVersion
import com.insidion.deployitnow.domain.events.ProjectVersionUpdatedEvent
import com.insidion.deployitnow.repository.ProjectVersionRepository
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.util.*

@Service
class ProjectVersionService(
        private val applicationEventPublisher: ApplicationEventPublisher,
        private val projectVersionRepository: ProjectVersionRepository
) {

    fun findByProjectAndNumber(project: Project, number: String): Optional<ProjectVersion> {
        return projectVersionRepository.findByProjectAndNumber(project.id, number)
    }

    fun updateVersion(project: Project, releaseName: String, description: String?, releaseDate: ZonedDateTime) {
        val pv: ProjectVersion = findByProjectAndNumber(project, releaseName).orElseGet {
            projectVersionRepository.save(ProjectVersion(releaseName,
                    project,
                    releaseDate))
        }
        val changed = pv.id == null || (pv.releaseNotes != description ?: "")
        if(changed) {
            pv.releaseNotes = description ?: ""
            val savedVersion = projectVersionRepository.save(pv)
            applicationEventPublisher.publishEvent(ProjectVersionUpdatedEvent(this, savedVersion))
        }
    }
}
