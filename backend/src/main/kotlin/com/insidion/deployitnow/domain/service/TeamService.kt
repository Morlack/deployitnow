package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.adapter.ws.WebsocketService
import com.insidion.deployitnow.domain.entities.NotificationType
import com.insidion.deployitnow.domain.entities.Team
import com.insidion.deployitnow.repository.TeamRepository
import org.springframework.stereotype.Service

@Service
class TeamService(
        val teamRepository: TeamRepository,
        val websocketService: WebsocketService,
) {
    fun getAll(): List<TeamDTO> {
        return teamRepository.findAll().map { TeamDTO(it) }
    }

    fun getById(id: Long) = teamRepository.findById(id)

    fun create(team: TeamDTO) {
        val team = teamRepository.save(Team(
                team.name,
                team.slackChannel,
                team.depedencyCheckInterval,
                team.notificationType,
        ))

        websocketService.sendTeamUpdated(team.id)
    }

    fun update(id: Long, dto: TeamDTO) {
        val team = teamRepository.findById(id).orElseThrow { IllegalStateException("No such team found") }
        team.name = dto.name;
        team.slackChannel = dto.slackChannel;
        team.notificationType = dto.notificationType;
        team.dependencyCheckInterval = dto.depedencyCheckInterval;
        teamRepository.save(team)
        websocketService.sendTeamUpdated(id)
    }
}

data class TeamDTO(
        val id: Long? = null,
        val name: String,
        val slackChannel: String,
        val depedencyCheckInterval: Int,
        val notificationType: NotificationType,
) {
    constructor(team: Team) : this(team.id, team.name, team.slackChannel, team.dependencyCheckInterval, team.notificationType)
}
