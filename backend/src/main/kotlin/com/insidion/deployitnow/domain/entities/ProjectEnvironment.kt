package com.insidion.deployitnow.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import com.insidion.deployitnow.infrastructure.config.JsonIntListConverter
import com.insidion.deployitnow.infrastructure.config.JsonStringListConverter
import com.vdurmont.semver4j.Semver
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import javax.persistence.*

@Entity
@Table(name = "project_environment")
class ProjectEnvironment {
    @Id
    @GeneratedValue(GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @ManyToOne()
    @JoinColumn(name = "project_id")
    @JsonIgnore
    var project: Project

    @Column(name = "name")
    var name: String

    @JoinColumn(name = "parent_id")
    @ManyToOne(fetch = FetchType.EAGER)
    var parent: ProjectEnvironment? = null;

    @OneToMany(mappedBy = "parent")
    var children: List<ProjectEnvironment> = emptyList()

    @ManyToOne
    @JoinColumn(name = "current_version_id")
    @JsonIgnore
    var currentVersion: ProjectVersion? = null

    @Column(name = "gitlab_id")
    var gitlabId: Int?

    @Column(name = "has_children")
    var hasChildren: Boolean = false

    constructor(project: Project, name: String, gitlabId: Int?, parent: ProjectEnvironment?) {
        this.project = project
        this.gitlabId = gitlabId
        this.name = name
        project.environments.plusAssign(this)
        this.parent = parent
    }

    fun isDeployed(version: Semver): Boolean {
        return if (currentVersion?.number?.isNotEmpty() == true) {
            version.isLowerThanOrEqualTo(currentVersion!!.number)
        } else false
    }

    fun isOfEnvironment(environment: String): Boolean {
        return if(parent != null) {
            name == environment || parent!!.isOfEnvironment(environment)
        } else {
            name == environment
        }
    }
}
