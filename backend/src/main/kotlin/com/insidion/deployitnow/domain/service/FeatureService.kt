package com.insidion.deployitnow.domain.service

import com.insidion.deployitnow.adapter.ws.WebsocketService
import com.insidion.deployitnow.domain.DeployItNowProperties
import com.insidion.deployitnow.domain.entities.Feature
import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.repository.FeatureRepository
import com.vdurmont.semver4j.Semver
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.util.*

@Service
class FeatureService(
        val featureRepository: FeatureRepository,
        val deployItNowProperties: DeployItNowProperties,
        val websocketService: WebsocketService,
) {
    fun getAllFeatures(): MutableList<Feature> {
        return featureRepository.findByArchivedAtNullOrderByCreatedAtDesc()
    }

    fun getFeatureById(id: Long): Optional<Feature> {
        return featureRepository.findById(id)
    }

    fun toggleArchiveById(id: Long) {
        val feature = featureRepository.findById(id).orElseThrow { throw IllegalStateException("Feature does not exist!") }
        if(feature.archivedAt != null ) {
            feature.archivedAt = null
        } else {
            feature.archivedAt = ZonedDateTime.now()
        }
        featureRepository.save(feature);
        sendFeatureUpdate(feature)
    }

    public fun sendFeatureUpdate(feature: Feature) {
        websocketService.sendFeatureUpdated(feature.id, feature.projectVersions.map { it.project.id })
    }

    fun getAllPendingFeaturesByProject(project: Project): List<Feature> {
        return featureRepository.findActiveByProject(project.id)
    }

    fun getAllFeaturesByProject(project: Project): List<Feature> {
        return featureRepository.findAllByProject(project.id)
    }

    fun getArchivedFeatures(page: Int = 0): MutableList<Feature> {
        return featureRepository.findByArchivedAtNotNullOrderByArchivedAtDesc(PageRequest.of(page, 50))
    }

    fun scanFeaturesForCompletion() {
        featureRepository.findByArchivedAtNullOrderByCreatedAtDesc().filter { feature ->
            feature.projectVersions
                    .groupBy { it.project }
                    .all { (project, projectVersions) ->
                        val version = projectVersions.maxByOrNull { Semver(it.number) }

                        val productionEnvironments = project.environments.filter { it.currentVersion != null && deployItNowProperties.archiveEnvironments.any { ae -> it.isOfEnvironment(ae) } }
                        if (productionEnvironments.isEmpty()) {
                            return@all false
                        }

                        productionEnvironments.all {
                            if (it.currentVersion?.number?.isNotEmpty() == true) {
                                Semver(version!!.number).isLowerThanOrEqualTo(it.currentVersion?.number)
                            } else false
                        }
                    }
        }.forEach {
            it.archivedAt = ZonedDateTime.now()
            featureRepository.save(it)
            sendFeatureUpdate(it)
        }
    }
}
