package com.insidion.deployitnow.domain.entities

import javax.persistence.*

@Entity
@Table(name = "dependency_provider")
class DependencyProvider {
    @Id
    @GeneratedValue(GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0


    @JoinColumn(name = "project_id")
    @ManyToOne(fetch = FetchType.EAGER)
    var project: Project

    @JoinColumn(name = "dependency_id")
    @ManyToOne(fetch = FetchType.EAGER)
    var dependency: Dependency


    constructor(project: Project, dependency: Dependency) {
        this.project = project;
        this.dependency = dependency;
    }

}
