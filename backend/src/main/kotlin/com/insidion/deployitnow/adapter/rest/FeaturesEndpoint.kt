package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.adapter.rest.model.FeatureDTO
import com.insidion.deployitnow.adapter.rest.model.FeatureProjectDTO
import com.insidion.deployitnow.adapter.rest.model.FeatureProjectEnvironmentDTO
import com.insidion.deployitnow.adapter.rest.model.TaskDTO
import com.insidion.deployitnow.domain.entities.Feature
import com.insidion.deployitnow.domain.entities.ProjectEnvironment
import com.insidion.deployitnow.domain.service.FeatureService
import com.vdurmont.semver4j.Semver
import org.springframework.web.bind.annotation.*
import java.time.ZonedDateTime

@RestController
@RequestMapping("api/features")
class FeaturesEndpoint(
        private val featureService: FeatureService
) {
    @GetMapping
    fun getAllFeatures(): List<FeatureDTO> {
        return featureService.getAllFeatures().map(this::createFeatureDto)
    }

    @GetMapping("/{featureId}")
    fun getSingleFeature(@PathVariable featureId: Long): FeatureDTO {
        return featureService.getFeatureById(featureId).map(this::createFeatureDto)
                .orElseThrow { IllegalStateException("Feature does not exist") }
    }

    @PostMapping("/{featureId}/archive")
    fun archiveFeature(@PathVariable featureId: Long) {
        featureService.toggleArchiveById(featureId);
    }

    @GetMapping("/archive")
    fun getAllFeatures(@RequestParam("page", defaultValue = "0") page: Int): List<FeatureDTO> {
        return featureService.getArchivedFeatures(page).map(this::createFeatureDto)
    }

    private fun createFeatureDto(feature: Feature): FeatureDTO {
        val projects = feature.projectVersions
                .groupBy { it.project }
                .map { (project, projectVersions) ->
                    val version = projectVersions.maxByOrNull { Semver(it.number) }
                    val mostRecentVersion = Semver(version!!.number)

                    val environmentDtos = project.environments.filter { !it.name.contains("/") }
                            .map {mapEnvironmentToDto(mostRecentVersion, it)}

                    FeatureProjectDTO(project.id, project.name, mostRecentVersion.originalValue, environmentDtos, ZonedDateTime.now())
                }
        return FeatureDTO(feature.id, feature.name, feature.issue, feature.lastUpdated, feature.archivedAt, projects, feature.tasks.map { TaskDTO(it) })
    }

    private fun mapEnvironmentToDto(mostRecentVersion: Semver, projectEnvironment: ProjectEnvironment): FeatureProjectEnvironmentDTO {
        val convertedChildren = projectEnvironment.children.map {mapEnvironmentToDto(mostRecentVersion, it)}

        return if(convertedChildren.isNotEmpty()) {
            FeatureProjectEnvironmentDTO(projectEnvironment.name, convertedChildren.all { it.deployed }, convertedChildren)
        } else {
            val isDeployed = projectEnvironment.isDeployed(mostRecentVersion)
            FeatureProjectEnvironmentDTO(projectEnvironment.name, isDeployed, convertedChildren)
        }
    }
}
