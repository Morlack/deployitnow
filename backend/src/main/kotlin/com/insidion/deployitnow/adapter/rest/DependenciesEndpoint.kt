package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.domain.service.DependencyService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("api/dependencies")
class DependenciesEndpoint(
        private val dependencyService: DependencyService
) {
    @GetMapping("overview")
    fun serveFrontend(): List<DependencyNode> {
        return dependencyService.getAll()
                .flatMap { it.dependencyProviders.flatMap { provider -> it.dependencyConsumers.map { consumer -> DependencyNode(it.type, it.name, provider.project.name, consumer.project.name) } } }
    }
}

data class DependencyNode(
        val type: String,
        val name: String,
        val origin: String,
        val destination: String,
)
