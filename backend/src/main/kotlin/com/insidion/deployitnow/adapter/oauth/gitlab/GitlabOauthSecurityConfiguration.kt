package com.insidion.deployitnow.adapter.oauth.gitlab

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver
import org.springframework.security.oauth2.core.AuthorizationGrantType
import org.springframework.security.oauth2.core.ClientAuthenticationMethod
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint
import org.springframework.security.web.util.matcher.AntPathRequestMatcher


@Configuration
@ConditionalOnProperty("deployitnow.oauth.gitlab.enabled", havingValue = "true")
class GitlabOauthSecurityConfiguration(
        val gitlabOauthProperties: GitlabOauthProperties
) : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        http
                .authorizeRequests()
                .requestMatchers(AntPathRequestMatcher("/api/tasks/**")).permitAll() // TODO: Need special api keys
                .requestMatchers(AntPathRequestMatcher("/api/ws")).permitAll()
                .requestMatchers(AntPathRequestMatcher("/api/**")).authenticated()
                .requestMatchers(AntPathRequestMatcher("/**")).permitAll()
                .and()
                .headers().frameOptions().disable()
                .and()
                .oauth2Login()
                .defaultSuccessUrl(gitlabOauthProperties.successUrl, true)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(Http403ForbiddenEntryPoint())
                .and().csrf().disable()
    }

    @Bean
    fun clientRegistrationRepository(): ClientRegistrationRepository {
        return InMemoryClientRegistrationRepository(this.gitlabClientRegistration())
    }

    private fun gitlabClientRegistration(): ClientRegistration {
        return ClientRegistration.withRegistrationId("gitlab")
                .clientId(gitlabOauthProperties.clientId)
                .clientSecret(gitlabOauthProperties.clientSecret)
                .clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUriTemplate("{baseUrl}/login/oauth2/code/{registrationId}")
                .scope("openid", "profile")
                .authorizationUri("${gitlabOauthProperties.baseUrl}/oauth/authorize")
                .tokenUri("${gitlabOauthProperties.baseUrl}/oauth/token")
                .userInfoUri("${gitlabOauthProperties.baseUrl}/oauth/userinfo")
                .jwkSetUri("${gitlabOauthProperties.baseUrl}/oauth/discovery/keys")
                .userNameAttributeName("name")
                .clientName("Gitlab")
                .build()
    }
}
