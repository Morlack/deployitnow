package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.domain.entities.TaskCompletionStatus
import com.insidion.deployitnow.domain.entities.TaskType

data class CreateEnvironmentRequest(
        val name: String,
        val jobs: String,
        val stage: String
)
data class UpdateEnvironmentRequest(
        val name: String,
        val jobs: String?,
        val stage: String,
        val order: Int,
        val enabled: Boolean
)

data class CreateTaskRequest(
        val featureId: Long,
        val type: TaskType,
        val remarks: String
)
data class CompleteTaskRequest(
        val completedStatus: TaskCompletionStatus,
        val remarks: String?
)
