package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.domain.DeployItNowProperties
import com.insidion.deployitnow.domain.entities.SettingName
import com.insidion.deployitnow.domain.service.SettingService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/settings")
class SettingsEndpoint(
        private val deployItNowProperties: DeployItNowProperties,
        private val settingService: SettingService
) {
    @GetMapping
    fun getSettings(): SettingsDTO {
        return SettingsDTO(deployItNowProperties.version, settingService.getAll().map { it.name.toString() to it.value }.toMap())
    }

    @PutMapping("/{settingName}")
    fun updateSetting(@PathVariable settingName: SettingName, @RequestBody update: SettingUpdateDTO) {
        settingService.storeValue(settingName, update.value)
    }
}


data class SettingsDTO(
        val version: String,
        val settingMap: Map<String, String?>
)

data class SettingUpdateDTO(
        val value: String?,
)
