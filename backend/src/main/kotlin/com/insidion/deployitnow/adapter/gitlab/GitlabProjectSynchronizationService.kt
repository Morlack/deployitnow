package com.insidion.deployitnow.adapter.gitlab

import com.fasterxml.jackson.databind.ObjectMapper
import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectEnvironment
import com.insidion.deployitnow.domain.entities.SettingName
import com.insidion.deployitnow.domain.service.DependencyService
import com.insidion.deployitnow.domain.service.SettingService
import com.insidion.deployitnow.repository.ProjectEnvironmentRepository
import com.insidion.deployitnow.repository.ProjectRepository
import org.slf4j.Logger
import org.springframework.stereotype.Service

@Service
class GitlabProjectSynchronizationService(
        private val logger: Logger,
        private val gitlabService: GitlabService,
        private val projectRepository: ProjectRepository,
        private val objectMapper: ObjectMapper,
        private val dependencyService: DependencyService,
        private val settingService: SettingService,
        private val gitlabReleaseSynchronizationService: GitlabReleaseSynchronizationService,
        private val projectEnvironmentRepository: ProjectEnvironmentRepository,
) {
    fun synchronizeProjects() {
        logger.info("Synchronizing projects")
        gitlabService.getAllProjects().forEach { gitlabProject ->
            try {
                synchronizeProject(gitlabProject)
            } catch (e: Exception) {
                logger.warn("Was unable to synchronize gitlab project {} ({}) because of exception", gitlabProject.id, gitlabProject.name, e)
            }
        }

        logger.info("Synchronized all projects")
    }

    fun synchronizeProject(projectId: Int) {
        val project = gitlabService.getProject(projectId)
        synchronizeProject(project)
    }

    private fun synchronizeProject(gitlabProject: org.gitlab4j.api.models.Project) {
        val archiveEnvs = settingService.getSettingListOfStrings(SettingName.GENERAL_ARCHIVE_ENVIRONMENTS)
        val whitelistedEnvs = settingService.getSettingListOfStrings(SettingName.GITLAB_ENVIRONMENTS_WHITELIST)
        logger.info("Synchronizing {}", gitlabProject.name)
        val project = projectRepository.findByGitlabId(gitlabProject.id)
                ?: projectRepository.save(Project(gitlabProject.name, gitlabProject.id))


        val environments = try {
            gitlabService.getEnvironments(gitlabProject.id)
        } catch (e: Exception) {
            emptyList()
        }


        environments
                .forEach { gitlabEnvironment ->
                    if (whitelistedEnvs.contains(gitlabEnvironment.name.split("/")[0])) {
                        val projectEnvironment = getOrCreateEnvironment(project, gitlabEnvironment.name);
                        projectEnvironment.gitlabId = gitlabEnvironment.id
                    }
                }

        projectRepository.save(project)
        synchronizeDependencies(project)
        if (project.hasEnvironments) {
            gitlabReleaseSynchronizationService.synchronizeReleasesForProject(project)
            gitlabReleaseSynchronizationService.synchronizeEnvironmentsForProject(project)
        }
        project.environments.removeIf {
            if (environments.none { gitlabEnv -> gitlabEnv.id == it.gitlabId }) {
                projectEnvironmentRepository.delete(it)
                true
            } else false
        }
        project.environments.groupBy { it.gitlabId }
                .filter { it.value.size > 1 }
                .forEach {
                    it.value.subList(1, it.value.size).forEach { pe ->
                        project.environments.remove(pe)
                        projectEnvironmentRepository.delete(pe)
                    }
                }
        project.hasEnvironments = !gitlabProject.archived && project.environments.any { archiveEnvs.contains(it.name) }
        logger.info("Synchronized {} with environments {}", gitlabProject.name, project.environments.joinToString { it.name })
    }

    fun synchronizeDependencies(project: Project) {
        if (!project.hasEnvironments) {
            return;
        }
        val json = gitlabService.getFileContent(projectId = project.gitlabId, "landscape.json") ?: return

        val (name, description, dependsOn, provides) = objectMapper.readValue(json, LandscapeModel::class.java)
        dependencyService.getProjectConsumingDependencies(project)
                .filter { dbDep -> dependsOn.none { defDep -> defDep.type == dbDep.dependency.name && defDep.name == dbDep.dependency.type } }
                .forEach { dependencyService.deleteDependency(it) }
        dependsOn.forEach { dependencyService.addConsumingDependencyToProject(project, it.type, it.name) }
        dependencyService.getProjectProvidingDependencies(project)
                .filter { dbDep -> provides.none { defDep -> defDep.type == dbDep.dependency.name && defDep.name == dbDep.dependency.type } }
                .forEach { dependencyService.deleteDependency(it) }
        provides.forEach { dependencyService.addProvidingDependencyToProject(project, it.type, it.name) }
    }


    fun getOrCreateEnvironment(project: Project, name: String): ProjectEnvironment {
        val environment = project.environments.find { it.name == name }
        if (environment != null) {
            return environment
        }

        val parts = name.split("/")
        val parent = if (parts.size > 1) getOrCreateEnvironment(project, parts.subList(0, parts.size - 1).joinToString("/")) else null
        if (parent != null) {
            parent.currentVersion = null
            parent.hasChildren = true
        }

        val projectEnvironment = projectEnvironmentRepository.save(ProjectEnvironment(project, name, null, parent))
        project.environments.plusAssign(projectEnvironment)
        return projectEnvironment
    }
}
