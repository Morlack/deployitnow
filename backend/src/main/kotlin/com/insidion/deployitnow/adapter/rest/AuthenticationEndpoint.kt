package com.insidion.deployitnow.adapter.rest

import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/auth")
class AuthenticationEndpoint {
    @GetMapping("whoami")
    fun getAllEnvironments(authentication: Authentication): String? {
        val token: OAuth2AuthenticationToken = authentication as OAuth2AuthenticationToken
        return token.name
    }
}
