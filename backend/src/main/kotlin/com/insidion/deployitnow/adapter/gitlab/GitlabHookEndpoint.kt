package com.insidion.deployitnow.adapter.gitlab

import org.gitlab4j.api.webhook.WebHookManager
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.POST
import javax.ws.rs.core.Context

@Service
@RequestMapping("/gitlab/hook/web")
class GitlabHookEndpoint (
        private val webHookManager: WebHookManager
) {
    @PostMapping
    fun handle(@Context request: HttpServletRequest) {
        webHookManager.handleRequest(request)
    }
}
