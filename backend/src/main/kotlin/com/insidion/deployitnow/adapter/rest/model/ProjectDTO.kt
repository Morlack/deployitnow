package com.insidion.deployitnow.adapter.rest.model

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectEnvironment
import com.insidion.deployitnow.domain.entities.ProjectVersion
import com.insidion.deployitnow.domain.service.TeamDTO
import java.time.ZonedDateTime

data class ProjectDTO(
        val id: Long,
        val name: String,
        val lastChecked: ZonedDateTime?,
        val environments: List<ProjectEnvironmentDTO>,
        val technologies: List<ProjectTechnologyDTO>,
        val team: TeamDTO?,
) {
    constructor(project: Project): this(
            project.id,
            project.name,
            project.lastChecked,
            project.environments.filter { !it.hasChildren}.map { ProjectEnvironmentDTO(it) },
            project.projectTechnologies.map { ProjectTechnologyDTO(it.name, it.filePath) },
            if(project.team != null) TeamDTO(project.team!!) else null
    )
}

data class ProjectTechnologyDTO(
        val technology: String,
        val file: String,
)


data class ProjectEnvironmentDTO(
        val name: String,
        val currentVersion: ProjectVersionDTO?
) {
    constructor(projectEnvironment: ProjectEnvironment): this(
            projectEnvironment.name,
            if(projectEnvironment.currentVersion != null) ProjectVersionDTO(projectEnvironment.currentVersion!!) else null
    )
}

data class ProjectVersionDTO(
        val versionNumber: String,
        val releaseNotes: String?,
        val releaseDate: ZonedDateTime
) {
    constructor(projectVersion: ProjectVersion) : this(
            projectVersion.number,
            projectVersion.releaseNotes,
            projectVersion.releaseDate
    )
}


data class ProjectDependenciesDTO(
        val name: String,
        val consumes: List<ProvidingDependencyDTO>,
        val provides: List<ConsumingDependencyDTO>,
)


data class ConsumingDependencyDTO(
        val type: String,
        val name: String,
        val consumedBy: List<DependencyProjectDTO>,
)

data class ProvidingDependencyDTO(
        val type: String,
        val name: String,
        val providedBy: List<DependencyProjectDTO>,
)

data class DependencyProjectDTO(
        val id: Long,
        val name: String,
)

data class RunningTaskDTO(
        val name: String,
        val start: ZonedDateTime,
)

data class PendingUpgradeDTO(
        var technology: String,
        var file: String,
        var type: String,
        var identifier: String,
        var currentVersion: String,
        var newVersion: String,
)

