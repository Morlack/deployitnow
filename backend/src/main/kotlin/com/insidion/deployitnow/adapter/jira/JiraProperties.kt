package com.insidion.deployitnow.adapter.jira

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@ConfigurationProperties(prefix = "deployitnow.jira")
@Component
@ConditionalOnProperty("deployitnow.jira.enabled", havingValue = "true")
class JiraProperties {
    lateinit var issueKey: String
}

