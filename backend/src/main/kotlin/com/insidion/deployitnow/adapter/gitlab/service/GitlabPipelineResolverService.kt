package com.insidion.deployitnow.adapter.gitlab.service

import com.insidion.deployitnow.adapter.gitlab.GitlabService
import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.service.PipelineResolverService
import org.springframework.stereotype.Service

@Service
class GitlabPipelineResolverService(
        val gitlabService: GitlabService
) : PipelineResolverService {
    override fun findPipelineUrlForProjectAndVersion(project: Project, version: String): String {
        return gitlabService.getPipelineForTag(project.gitlabId, version)?.webUrl
                ?: throw IllegalStateException("Can not find pipeline for project ${project.name}")
    }

}
