package com.insidion.deployitnow.adapter.gitlab

import com.insidion.deployitnow.domain.entities.SettingName
import com.insidion.deployitnow.domain.service.SettingService
import com.insidion.deployitnow.infrastructure.config.withBlock
import io.micrometer.core.instrument.Metrics
import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.*
import org.springframework.stereotype.Service


@Service
class GitlabService(
        private val settingService: SettingService,
) {
    private val getProjectsTimer = Metrics.timer("gitlab.projects.timer")
    private val getEnvironmentsTimer = Metrics.timer("gitlab.envs.timer")
    private val getEnvironmentTimer = Metrics.timer("gitlab.env.timer")
    private val getTagsTimer = Metrics.timer("gitlab.tags.timer")
    private val getPipelineTimer = Metrics.timer("gitlab.pipeline.timer")

    fun getApi(): GitLabApi {
        return GitLabApi(settingService.getSettingString(SettingName.GITLAB_URL), settingService.getSettingString(SettingName.GITLAB_API_TOKEN))
    }


    fun getAllProjects(): List<Project> = getProjectsTimer.withBlock {
        getApi().projectApi.projects
    }

    fun getProject(projectId: Int): Project = getApi().projectApi.getProject(projectId)

    fun getEnvironments(projectId: Int): MutableList<Environment> = getEnvironmentsTimer.withBlock {
        getApi().environmentsApi.getEnvironments(projectId)
    }


    fun getEnvironment(projectId: Int, environmentId: Int): Environment = getEnvironmentTimer.withBlock {
        getApi().environmentsApi.getEnvironment(projectId, environmentId)
    }

    fun getRecentTags(projectId: Int, pageSize: Int = 20): List<Tag> = getTagsTimer.withBlock {
        getApi().tagsApi.getTags(projectId, 0, pageSize)
    }

    fun getPipelineForTag(projectId: Int, tagName: String) = getPipelineTimer.withBlock {
        getApi().pipelineApi.getPipelines(
                projectId,
                Constants.PipelineScope.TAGS,
                null,
                tagName,
                false,
                null,
                null,
                Constants.PipelineOrderBy.ID,
                Constants.SortOrder.DESC,
                0,
                1).firstOrNull()
    }

    fun getFileContent(projectId: Int, filePath: String, ref: String = "master"): String? {
        return try {
            getApi().repositoryFileApi.getFile(projectId, filePath, ref)?.decodedContentAsString
        } catch(e: Exception) {
            null
        }
    }
    fun getFile(projectId: Int, filePath: String, ref: String = "master"): RepositoryFile? {
        return try {
            getApi().repositoryFileApi.getFile(projectId, filePath, ref)
        } catch(e: Exception) {
            null
        }
    }

    fun updateFileInCommitOnNewBranchFromMaster(projectId: Int, branch: String, filePath: String, content: String, commitMessage: String) {
        getApi().commitsApi.createCommit(projectId, branch, commitMessage, "master", "micromanage@local", "MicroManage",
                CommitAction().withAction(CommitAction.Action.UPDATE)
                        .withEncoding(Constants.Encoding.TEXT)
                        .withContent(content)
                        .withFilePath(filePath)
        )
    }

    fun updateFilesInCommitOnNewBranchFromMaster(projectId: Int, branch: String, commitMessage: String, fileUpdates: Map<String, String>, startBranch: String? = null) {
        getApi().commitsApi.createCommit(projectId, branch, commitMessage, startBranch, "micromanage@local", "MicroManage",
                fileUpdates.map { CommitAction().withAction(CommitAction.Action.UPDATE)
                        .withEncoding(Constants.Encoding.BASE64)
                        .withContent(it.value)
                        .withFilePath(it.key) }
        )
    }

    fun createMergeRequest(projectId: Int, branch: String, title: String): MergeRequest {
        return getApi().mergeRequestApi.createMergeRequest(projectId, branch, "master", title, null, null)
    }

    fun getMergeRequest(projectId: Int, mergeRequestIid: Int): MergeRequest {
        return getApi().mergeRequestApi.getMergeRequest(projectId, mergeRequestIid)
    }
}
