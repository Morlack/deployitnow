package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.domain.service.TeamDTO
import com.insidion.deployitnow.domain.service.TeamService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/teams")
class TeamsEndpoint(
        private val teamService: TeamService
) {
    @GetMapping
    fun getAllTasks() = teamService.getAll()

    @PostMapping
    fun createTeam(@RequestBody teamDTO: TeamDTO) = teamService.create(teamDTO)

    @PutMapping("/{id}")
    fun updateTeam(@RequestBody teamDTO: TeamDTO, @PathVariable id: Long) = teamService.update(id, teamDTO)

}
