package com.insidion.deployitnow.adapter.ws

import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class WebsocketService(
        val simpMessagingTemplate: SimpMessagingTemplate
) {
    fun sendProjectUpdated(id: Long) {
        simpMessagingTemplate.convertAndSend("/topic/projects", ProjectUpdatedEvent(id))
    }

    fun sendFeatureUpdated(id: Long, projectIds: List<Long>) {
        simpMessagingTemplate.convertAndSend("/topic/features", FeatureUpdatedEvent(id, projectIds))
    }

    fun sendTeamUpdated(id: Long) {
        simpMessagingTemplate.convertAndSend("/topic/teams", TeamUpdatedEvent(id))
    }
}
