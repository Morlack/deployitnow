package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.domain.entities.upgrade.UpgradeItem
import com.insidion.deployitnow.domain.service.UpgradeService
import com.insidion.deployitnow.repository.UpgradeRepository
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("api/upgrades")
class UpgradesEndpoint(
        private val upgradeRepository: UpgradeRepository,
        private val upgradeService: UpgradeService,
) {
    @GetMapping
    fun getAllUpgradesByProject(): List<PendingUpgradesForProjectDTO> {
        return upgradeRepository.findAllByClosedFalse()
                .groupBy { it.project }
                .map { it.value }
                .map {
                    PendingUpgradesForProjectDTO(
                            PendingUpgradeProjectDTO(
                                    it[0].project.name,
                                    it[0].project.id,
                                    it[0].mergeRequestUrl
                            ),
                            it.flatMap { upgrade ->
                                upgrade.items.map { item -> PendingUpgradeDTO(item.technology, item.file, item) }
                            })
                }
    }

    @PostMapping("mr/{projectId}/close")
    fun closeUpgradeForProject(@PathVariable projectId: Long) {
        upgradeService.markUpgradesForProjectAsDone(projectId)
    }
}

data class PendingUpgradesForProjectDTO(
        val project: PendingUpgradeProjectDTO,
        val upgrades: List<PendingUpgradeDTO>
)

data class PendingUpgradeDTO(
        val technology: String,
        val file: String,
        val type: String,
        val identifier: String,
        val currentVersion: String,
        val availableVersion: String
) {
    constructor(technologyName: String, file: String, item: UpgradeItem) : this(technologyName, file, item.type, item.identifier, item.currentVersion, item.availableVersion)
}

data class PendingUpgradeProjectDTO(
        val name: String,
        val id: Long,
        val mrUrl: String?,
)
