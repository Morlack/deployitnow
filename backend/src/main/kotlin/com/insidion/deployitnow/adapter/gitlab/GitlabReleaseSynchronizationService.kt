package com.insidion.deployitnow.adapter.gitlab

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.SettingName
import com.insidion.deployitnow.domain.service.ProjectVersionService
import com.insidion.deployitnow.domain.service.SettingService
import com.insidion.deployitnow.repository.ProjectEnvironmentRepository
import org.gitlab4j.api.GitLabApiException
import org.slf4j.Logger
import org.springframework.stereotype.Service
import java.time.ZoneId
import java.time.ZonedDateTime

@Service
class GitlabReleaseSynchronizationService(
        private val gitlabService: GitlabService,
        private val projectVersionService: ProjectVersionService,
        private val logger: Logger,
        private val settingService: SettingService,
        val projectEnvironmentRepository: ProjectEnvironmentRepository
) {

    fun synchronizeReleasesForProject(project: Project) {
        withErrorHandlingAndSetting(project) {
            gitlabService.getRecentTags(project.gitlabId).forEach { tag ->
                val releaseDate = ZonedDateTime.ofInstant(tag.commit?.committedDate?.toInstant(), ZoneId.systemDefault()) ?: ZonedDateTime.now()
                projectVersionService.updateVersion(project,
                        tag.name,
                        tag.release?.description,
                        releaseDate)
            }
        }
    }

    fun synchronizeEnvironmentsForProject(project: Project) {
        withErrorHandlingAndSetting(project) {
            project.environments
                    // Retrieve gitlab environment fully, so we can see the last deployment
                    .filter { it.gitlabId != null && !it.hasChildren }
                    .map { projectEnvironment -> projectEnvironment to gitlabService.getEnvironment(project.gitlabId, projectEnvironment.gitlabId!!) }
                    .filter { (_, environment) -> environment.lastDeployment != null }
                    .forEach { (projectEnvironment, environment) ->
                        val current = projectVersionService.findByProjectAndNumber(project, environment.lastDeployment.ref).orElse(null)
                        projectEnvironment.currentVersion = current
                        projectEnvironmentRepository.save(projectEnvironment)
                    }
        }
    }

    fun withErrorHandlingAndSetting(project: Project, block: (Project) -> Unit) {
        if (!settingService.getSettingBool(SettingName.GITLAB_ENABLED)) {
            return
        }
        try {
            block(project)
        } catch (e: Exception) {
            logger.error("Could not synchronize project (id={}, gitlabId={}, name={}) because of error", project.id, project.gitlabId, project.name, e)

            if (e is GitLabApiException && e.httpStatus == 404) {
                project.hasEnvironments = false
            }
        }
    }
}
