package com.insidion.deployitnow.adapter.oauth.gitlab

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@ConfigurationProperties(prefix = "deployitnow.oauth.gitlab")
@Component
@ConditionalOnProperty("deployitnow.oauth.gitlab.enabled", havingValue = "true")
class GitlabOauthProperties {
    lateinit var baseUrl: String
    lateinit var clientId: String
    lateinit var clientSecret: String
    lateinit var successUrl: String
}
