package com.insidion.deployitnow.adapter.rest.model

import com.insidion.deployitnow.domain.entities.Feature
import com.insidion.deployitnow.domain.entities.Project
import java.time.ZonedDateTime

data class FeatureSummaryDTO(
        val featureId: Long,
        val name: String,
        val issue: String?,
        val versions: List<String>,
        val lastUpdated: ZonedDateTime,
        val archivedAt: ZonedDateTime?
) {
    constructor(feature: Feature, project: Project?) : this(feature.id, feature.name, feature.issue, feature.projectVersions.filter{ project == null || it.project.id == project.id  }.map { it.number }, feature.lastUpdated, feature.archivedAt)
}
data class FeatureDTO(
        val featureId: Long,
        val name: String,
        val issue: String?,
        val lastUpdated: ZonedDateTime,
        val archivedAt: ZonedDateTime?,
        val projects: List<FeatureProjectDTO>,
        val tasks: List<TaskDTO>
)

data class FeatureProjectDTO(
        val projectId: Long,
        val projectName: String,
        val versionNumber: String,
        val environments: List<FeatureProjectEnvironmentDTO>,
        val releaseDate: ZonedDateTime
)

data class FeatureProjectEnvironmentDTO(
        val name: String,
        val deployed: Boolean,
        val children: List<FeatureProjectEnvironmentDTO> = emptyList()
)
