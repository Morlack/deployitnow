package com.insidion.deployitnow.adapter.slack

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component


@ConfigurationProperties(prefix = "deployitnow.slack")
@Component
class SlackProperties {
    lateinit var webhookUrl: String
}
