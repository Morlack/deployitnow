package com.insidion.deployitnow.adapter.gitlab

import com.insidion.deployitnow.domain.service.ProjectService
import com.insidion.deployitnow.domain.service.UpgradeService
import org.gitlab4j.api.webhook.MergeRequestEvent
import org.gitlab4j.api.webhook.PipelineEvent
import org.gitlab4j.api.webhook.WebHookListener
import org.gitlab4j.api.webhook.WebHookManager
import org.slf4j.Logger
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class WebhookService(
        val logger: Logger,
        val webHookManager: WebHookManager,
        val upgradeService: UpgradeService,
        val projectService: ProjectService,
        val gitlabReleaseSynchronizationService: GitlabReleaseSynchronizationService,
) : WebHookListener {

    @PostConstruct
    fun setup() {
        webHookManager.addListener(this)
    }

    override fun onMergeRequestEvent(event: MergeRequestEvent?) {
        if (event == null || event.objectAttributes == null) {
            return;
        }
        logger.info("Received MR event for projectID {} and MrIId {} with state {}", event.objectAttributes.sourceProjectId, event.objectAttributes.iid, event.objectAttributes.state)
        if (event.objectAttributes.state != "opened") {
            upgradeService.markUpgradesWithMergeRequestAsDone(event.objectAttributes.sourceProjectId, event.objectAttributes.iid)
        }
    }

    override fun onPipelineEvent(event: PipelineEvent?) {
        if (event == null || event.objectAttributes == null) {
            return;
        }
        val project = projectService.getProjectByGitlabId(event.project.id) ?: return
        logger.info("Got pipeline event for project {}, refreshing environments", project.name)
        if (event.objectAttributes.status != "success") {
            logger.info("Not refreshing since wasn't success")
            return
        }
        if (!project.hasEnvironments) {
            logger.info("Not refreshing since project has no environments..")
            return;
        }
        gitlabReleaseSynchronizationService.synchronizeEnvironmentsForProject(project)
    }
}
