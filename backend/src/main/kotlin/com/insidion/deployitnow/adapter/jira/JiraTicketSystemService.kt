package com.insidion.deployitnow.adapter.jira

import com.insidion.deployitnow.domain.entities.Feature
import com.insidion.deployitnow.domain.service.TicketSystemService
import com.insidion.deployitnow.repository.FeatureRepository
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import javax.annotation.PostConstruct

@Service
@ConditionalOnProperty("deployitnow.jira.enabled", havingValue = "true")
class JiraTicketSystemService(
        val jiraProperties: JiraProperties,
        val featureRepository: FeatureRepository
) : TicketSystemService {
    private lateinit var jiraIssueRegex: Regex
    private lateinit var jiraCleanRegex: Regex

    @PostConstruct
    fun setup() {
        this.jiraIssueRegex = Regex("${jiraProperties.issueKey}-\\d+")
        this.jiraCleanRegex = Regex("\\[*${jiraProperties.issueKey}-\\d+\\]*")
    }

    override fun findFeatureForReleaseNote(releaseNote: String): Feature? {
        val jiraIssue = jiraIssueRegex.find(releaseNote)?.value
        return if (jiraIssue != null) {
            featureRepository.findByIssue(jiraIssue)
        } else {
            featureRepository.findFirstByName(releaseNote)
        }.orElseGet {
            Feature(name = releaseNote.replace(jiraCleanRegex, "").trim(), issue = jiraIssue, createdAt = ZonedDateTime.now())
        }
    }

    override fun ticketSystemName(): String {
        return "jira"
    }

}
