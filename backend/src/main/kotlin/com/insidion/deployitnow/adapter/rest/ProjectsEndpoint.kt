package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.adapter.gitlab.GitlabProjectSynchronizationService
import com.insidion.deployitnow.adapter.gitlab.GitlabReleaseSynchronizationService
import com.insidion.deployitnow.adapter.rest.model.*
import com.insidion.deployitnow.adapter.rest.model.PendingUpgradeDTO
import com.insidion.deployitnow.domain.entities.TaskResult
import com.insidion.deployitnow.domain.service.FeatureService
import com.insidion.deployitnow.domain.service.PipelineResolverService
import com.insidion.deployitnow.domain.service.ProjectService
import com.insidion.deployitnow.domain.service.TaskService
import com.insidion.deployitnow.domain.tasks.DetectorTask
import com.insidion.deployitnow.domain.tasks.ScannerTask
import com.insidion.deployitnow.repository.ProjectRepository
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/projects")
class ProjectsEndpoint(
        private val projectRepository: ProjectRepository,
        private val pipelineResolverService: PipelineResolverService,
        private val projectService: ProjectService,
        private val featureService: FeatureService,
        private val detectorTask: DetectorTask,
        private val scannerTask: ScannerTask,
        private val taskService: TaskService,
        private val projectSynchronizationService: GitlabProjectSynchronizationService,
) {
    @GetMapping
    fun getAllProjects() = projectRepository.findAll()
            .map { ProjectDTO(it) }

    @GetMapping("active")
    fun getAllActiveProjects() = projectRepository.findByHasEnvironmentsTrue()
            .map { ProjectDTO(it) }

    @GetMapping("inactive")
    fun getAllInactiveProjects() = projectRepository.findByHasEnvironmentsFalse()
            .map { ProjectDTO(it) }

    @GetMapping("/{projectId}/features")
    fun getSingleProject(@PathVariable projectId: Long): List<FeatureSummaryDTO> {
        val project = projectRepository.findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
        return featureService.getAllFeaturesByProject(project)
                .map { FeatureSummaryDTO(it, project) }
    }

    @GetMapping("/{projectId}")
    fun getFeaturesForProject(@PathVariable projectId: Long): ProjectDTO {
        return projectRepository
                .findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
                .run { ProjectDTO(this) }
    }

    @PutMapping("/{projectId}/set-team/{teamId}")
    fun setTeamForProject(@PathVariable projectId: Long, @PathVariable teamId: Long) {
        return projectService.setTeamForProject(projectId, teamId)
    }

    @GetMapping("/{projectId}/dependencies")
    fun getDependenciesOfProject(@PathVariable projectId: Long): ProjectDependenciesDTO {
        return projectRepository
                .findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
                .run {
                    ProjectDependenciesDTO(
                            this.name,
                            this.consumedDependencies.map { ProvidingDependencyDTO(it.dependency.type, it.dependency.name, it.dependency.dependencyProviders.map { provider -> DependencyProjectDTO(provider.project.id, provider.project.name) }) },
                            this.providedDependencies.map { ConsumingDependencyDTO(it.dependency.type, it.dependency.name, it.dependency.dependencyConsumers.map { provider -> DependencyProjectDTO(provider.project.id, provider.project.name) }) },
                    )
                }

    }

    @PostMapping("/{projectId}/sync")
    fun synchronizeProject(@PathVariable projectId: Long) {
        val project = projectRepository
                .findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
        projectSynchronizationService.synchronizeProject(project.gitlabId)
    }

    @GetMapping("/{projectId}/pipeline/{projectVersion}")
    fun getPipelineUrl(@PathVariable projectId: Long, @PathVariable projectVersion: String): String? {
        val project = projectRepository.findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
        return pipelineResolverService.findPipelineUrlForProjectAndVersion(project, projectVersion)
    }


    @GetMapping("/{projectId}/conflicts")
    fun getFeaturesDeployedWhenVersionIs(@PathVariable projectId: Long, @RequestParam(name = "version") version: String, @RequestParam(name = "environment") envName: String): List<FeatureSummaryDTO> {
        val project = projectRepository.findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
        return projectService.getFeaturesNotOnEnvironmentBeforeVersion(project, envName, version)
                .map { FeatureSummaryDTO(it, project) }
    }

    @PostMapping("/{projectId}/task/detector/start")
    fun startDetectorTaskForProject(@PathVariable projectId: Long) {
        val project = projectRepository.findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
        return detectorTask.startForProject(project)
    }

    @PostMapping("/{projectId}/task/scanner/start")
    fun startScannerTaskForProject(@PathVariable projectId: Long) {
        val project = projectRepository.findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
        scannerTask.startForProject(project);
    }

    @GetMapping("/{projectId}/tasks/running")
    fun getRunningTasksForProject(@PathVariable projectId: Long): List<RunningTaskDTO> {
        val project = projectRepository.findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
        return taskService.retrieveRunningTasksForProject(project).map { RunningTaskDTO(it.name, it.started) }
    }


    @PostMapping("/{projectId}/tasks/{name}/end")
    fun endRunningTaskForProject(@PathVariable projectId: Long, @PathVariable name: String) {
        val project = projectRepository.findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
        return taskService.registerResultForTask(project, name, TaskResult.CANCELLED_MANUALLY)
    }

    @GetMapping("/{projectId}/pendingupgrades")
    fun getPendingUpdatesForProject(@PathVariable projectId: Long): List<PendingUpgradeDTO> {
        val project = projectRepository.findById(projectId).orElseThrow { IllegalStateException("Project does not exist") }
//        return projectPendingUpgradeRepository.findAllByProject(project).map { PendingUpgradeDTO(it, it.file, it.type, it.dependency, it.oldVersion, it.newVersion) }
        return emptyList()
    }
}
