package com.insidion.deployitnow.adapter.ws

import com.insidion.deployitnow.domain.entities.SettingName

data class ProjectUpdatedEvent(
        val projectId: Long
)

data class TeamUpdatedEvent(
        val projectId: Long
)

data class FeatureUpdatedEvent(
        val featureId: Long,
        val projectIds: List<Long>
)

data class SettingUpdatedEvent(
        val settingName: SettingName
)
