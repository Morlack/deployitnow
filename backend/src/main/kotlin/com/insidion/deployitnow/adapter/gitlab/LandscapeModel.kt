package com.insidion.deployitnow.adapter.gitlab

data class LandscapeModel(
        val name: String,
        val description: String?,
        val dependsOn: List<DependencyDefinition>,
        val provides: List<DependencyDefinition>,
)

data class DependencyDefinition(
        val type: String,
        val name: String,
        val description: String?,
)
