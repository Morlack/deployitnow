package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.adapter.rest.model.TaskDTO
import com.insidion.deployitnow.domain.entities.Task
import com.insidion.deployitnow.repository.FeatureRepository
import com.insidion.deployitnow.repository.TaskRepository
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.web.bind.annotation.*
import java.time.ZonedDateTime

@RestController
@RequestMapping("api/tasks")
class TasksEndpoint(
        private val taskRepository: TaskRepository,
        private val featureRepository: FeatureRepository
) {
    @GetMapping
    fun getAllTasks() = taskRepository
            .findByCompletedAtNull()
            .map {TaskDTO(it)}

    @PostMapping
    fun createTask(@RequestBody createTaskRequest: CreateTaskRequest, authentication: Authentication): TaskDTO {
        val feature = featureRepository.findById(createTaskRequest.featureId).orElseThrow { IllegalStateException("Feature does not exist") }
        val token: OAuth2AuthenticationToken = authentication as OAuth2AuthenticationToken
        val task = Task(
                feature = feature,
                requestedBy = token.name,
                requestedRemarks = createTaskRequest.remarks,
                type = createTaskRequest.type,
                requestedAt = ZonedDateTime.now())

        return TaskDTO(taskRepository.save(task))
    }

    @PutMapping("{id}/complete")
    fun completeTask(@PathVariable id: Long, @RequestBody completeTaskRequest: CompleteTaskRequest, authentication: Authentication): TaskDTO {
        val task = taskRepository.findById(id).orElseThrow { IllegalStateException("Task does not exist") }
        require(task.completedAt == null) {IllegalStateException("Task is already completed")}

        val token: OAuth2AuthenticationToken = authentication as OAuth2AuthenticationToken

        task.completedAt = ZonedDateTime.now()
        task.completedBy = token.name
        task.completedRemarks = completeTaskRequest.remarks
        task.status = completeTaskRequest.completedStatus

        return TaskDTO(taskRepository.save(task))
    }
}
