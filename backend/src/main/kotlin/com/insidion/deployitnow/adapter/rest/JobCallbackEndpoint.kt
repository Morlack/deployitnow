package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.domain.tasks.DependencyUpgradeCallbackFile
import com.insidion.deployitnow.domain.tasks.DetectorTask
import com.insidion.deployitnow.domain.tasks.ScannerTask
import com.insidion.deployitnow.domain.tasks.TechnologyDetectorCallbackItem
import org.slf4j.Logger
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/josbs/callback")
class JobCallbackEndpoint(private val logger: Logger,
                          private val detectorTask: DetectorTask,
                          private val scannerTask: ScannerTask) {
    @PostMapping("scanner/technology")
    fun technologyDetectorCallback(@RequestParam projectId: Long, @RequestBody technologies: List<TechnologyDetectorCallbackItem>) {
        logger.info("Received for project {}: {}", projectId, technologies)
        detectorTask.handleTechnologies(projectId, technologies)
    }

    @PostMapping("scanner/upgrades/{projectId}/{technology}")
    fun dependencyResultCallback(@PathVariable projectId: Long, @PathVariable technology: String, @RequestBody technologies: List<DependencyUpgradeCallbackFile>) {
        logger.info("Received for project {}({}): {}", projectId, technology, technologies)
        scannerTask.handleResult(projectId, technology, technologies)
    }
}
