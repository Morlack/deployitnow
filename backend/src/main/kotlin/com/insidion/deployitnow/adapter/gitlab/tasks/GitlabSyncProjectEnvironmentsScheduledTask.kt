package com.insidion.deployitnow.adapter.gitlab.tasks

import com.insidion.deployitnow.adapter.gitlab.GitlabProjectSynchronizationService
import com.insidion.deployitnow.domain.entities.SettingName
import com.insidion.deployitnow.domain.service.SettingService
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class GitlabSyncProjectEnvironmentsScheduledTask(
        private val gitlabProjectSynchronizationService: GitlabProjectSynchronizationService,
        private val settingService: SettingService,
) {

    @Scheduled(fixedDelay = 30 * 60 * 1000)
    fun synchronizeProjects() {
        if(settingService.getSettingBool(SettingName.GITLAB_ENABLED) && settingService.getSettingBool(SettingName.GITLAB_ENABLE_PROJECT_SCAN)) {
            gitlabProjectSynchronizationService.synchronizeProjects()
        }
    }
}
