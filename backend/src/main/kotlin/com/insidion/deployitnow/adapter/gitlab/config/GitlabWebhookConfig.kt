package com.insidion.deployitnow.adapter.gitlab.config

import org.gitlab4j.api.webhook.WebHookManager
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class GitlabWebhookConfig(
) {
    @Bean
    fun webhookManager(): WebHookManager{
        // TODO: Secret token verification
        return WebHookManager()
    }
}
