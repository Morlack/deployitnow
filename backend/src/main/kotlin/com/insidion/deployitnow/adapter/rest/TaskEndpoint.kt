package com.insidion.deployitnow.adapter.rest

import com.insidion.deployitnow.domain.tasks.*
import org.slf4j.Logger
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/tasks")
class TaskEndpoint(private val logger: Logger,
                   private val detectorTask: DetectorTask,
                   private val scannerTask: ScannerTask) {
    @PostMapping("detector/technology")
    fun technologyDetectorCallback(@RequestParam projectId: Long, @RequestBody technologies: List<TechnologyDetectorCallbackItem>) {
        logger.info("Received for project {}: {}", projectId, technologies)
        detectorTask.handleTechnologies(projectId, technologies)
    }

    @PostMapping("dependency/result/{projectId}/{technology}")
    fun dependencyResultCallback(@PathVariable projectId: Long, @PathVariable technology: String, @RequestBody technologies: List<DependencyUpgradeCallbackFile>) {
        logger.info("Received for project {}({}): {}", projectId, technology, technologies)
        scannerTask.handleResult(projectId, technology, technologies)
    }
}
