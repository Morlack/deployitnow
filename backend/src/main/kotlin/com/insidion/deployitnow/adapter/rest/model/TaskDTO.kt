package com.insidion.deployitnow.adapter.rest.model

import com.insidion.deployitnow.domain.entities.Task
import com.insidion.deployitnow.domain.entities.TaskCompletionStatus
import com.insidion.deployitnow.domain.entities.TaskType
import java.time.ZonedDateTime

data class TaskDTO(
        val id: Long,
        val featureId: Long,
        val type: TaskType,
        val requestedBy: String,
        val requestedRemarks: String?,
        val requestedAt: ZonedDateTime,
        val completedBy: String?,
        val completedRemarks: String?,
        val completedAt: ZonedDateTime?,
        val completedStatus: TaskCompletionStatus?

) {
    constructor(task: Task) :
            this(task.id!!, task.feature.id!!, task.type, task.requestedBy, task.requestedRemarks, task.requestedAt,
                    task.completedBy, task.completedRemarks, task.completedAt, task.status)
}
