package com.insidion.deployitnow.adapter.slack

import com.slack.api.Slack
import com.slack.api.webhook.Payload
import org.springframework.stereotype.Service

@Service
class SlackService(
        val slackProperties: SlackProperties
) {

    fun sendMessage(payload: Payload) {
        Slack.getInstance().send(slackProperties.webhookUrl, payload)
    }
}
