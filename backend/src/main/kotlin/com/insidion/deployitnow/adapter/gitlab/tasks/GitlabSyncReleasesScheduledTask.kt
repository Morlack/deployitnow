package com.insidion.deployitnow.adapter.gitlab.tasks

import com.insidion.deployitnow.adapter.gitlab.GitlabReleaseSynchronizationService
import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.infrastructure.config.withBlock
import com.insidion.deployitnow.repository.ProjectRepository
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import org.springframework.data.domain.PageRequest
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.concurrent.atomic.AtomicLong
import javax.annotation.PostConstruct

@Component
class GitlabSyncReleasesScheduledTask(
        private val meterRegistry: MeterRegistry,
        private val projectRepository: ProjectRepository,
        private val gitlabReleaseSynchronizationService: GitlabReleaseSynchronizationService
) {
    private val timeBetweenSyncsGauge = AtomicLong()
    private lateinit var syncTimeTimer: Timer


    @PostConstruct
    fun setup() {
        meterRegistry.gauge("deployitnow.projects.gitlab.refresh", timeBetweenSyncsGauge)
        syncTimeTimer = meterRegistry.timer("deployitnow.sync.timer")
    }

    @Scheduled(fixedDelay = 1000)
    fun synchronizeProjects() {
        projectRepository
                .findByHasEnvironmentsTrueAndLastCheckedBeforeOrderByLastCheckedAsc(ZonedDateTime.now().minusMinutes(2), PageRequest.of(0, 10))
                .parallelStream()
                .forEach(this::synchronizeProject)
    }

    private fun synchronizeProject(project: Project) {
        syncTimeTimer.withBlock {
            gitlabReleaseSynchronizationService.synchronizeReleasesForProject(project)

            if (project.lastChecked != null) {
                timeBetweenSyncsGauge.set(ChronoUnit.SECONDS.between(project.lastChecked, ZonedDateTime.now()))
            }
            project.lastChecked = ZonedDateTime.now()
            projectRepository.save(project)
        }
    }
}
