package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Team
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component

@Component
interface TeamRepository : JpaRepository<Team, Long> {
}
