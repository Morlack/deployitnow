package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Task
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component

@Component
interface TaskRepository : JpaRepository<Task, Long> {
    fun findByCompletedAtNull(): List<Task>
}
