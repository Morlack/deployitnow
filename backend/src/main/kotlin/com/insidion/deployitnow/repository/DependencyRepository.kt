package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Dependency
import com.insidion.deployitnow.domain.entities.DependencyConsumer
import com.insidion.deployitnow.domain.entities.DependencyProvider
import com.insidion.deployitnow.domain.entities.Project
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component
import java.time.ZonedDateTime

@Component
interface DependencyRepository : JpaRepository<Dependency, Long> {
    fun findByTypeAndName(type: String, name: String): Dependency?
}
@Component
interface DependencyProviderRepository : JpaRepository<DependencyProvider, Long> {
    fun findByProjectAndDependency(project: Project, dependency: Dependency): DependencyProvider?
    fun findAllByProject(project: Project): List<DependencyProvider>
}

@Component
interface DependencyConsumerRepository : JpaRepository<DependencyConsumer, Long> {
    fun findByProjectAndDependency(project: Project, dependency: Dependency): DependencyConsumer?
    fun findAllByProject(project: Project): List<DependencyConsumer>
}
