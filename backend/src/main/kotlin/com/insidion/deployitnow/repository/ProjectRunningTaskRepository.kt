package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectRunningTask
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Component
import java.time.ZonedDateTime

@Component
interface ProjectRunningTaskRepository : JpaRepository<ProjectRunningTask, Long> {
    fun findByProjectAndNameAndFinishedNull(project: Project, name: String): ProjectRunningTask?
    fun findByProjectAndFinishedNull(project: Project): List<ProjectRunningTask>
    fun findByFinishedNullAndStartedBefore(started: ZonedDateTime): List<ProjectRunningTask>

    @Query("select max(t.started) from ProjectRunningTask  t where t.project=:project AND t.name=:type")
    fun findLatestByProject(@Param(value = "project") project: Project, @Param(value = "type") type: String): ZonedDateTime?
}
