package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.ProjectEnvironment
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component

@Component
interface ProjectEnvironmentRepository : JpaRepository<ProjectEnvironment, Long> {
}
