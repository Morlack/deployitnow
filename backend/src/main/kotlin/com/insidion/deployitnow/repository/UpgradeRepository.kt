package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.upgrade.Upgrade
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
@Transactional
interface UpgradeRepository : JpaRepository<Upgrade, Long> {
    fun findAllByProject(project: Project): List<Upgrade>
    fun findAllByClosedFalse(): List<Upgrade>
    fun findByProjectAndClosedFalse(project: Project): Upgrade?
    fun deleteAllByProject(project: Project): List<Upgrade>
    fun findByProjectGitlabIdAndClosedFalseAndMergeRequestId(gitlabId: Int, mergeRequestId: Int): Upgrade?
}
