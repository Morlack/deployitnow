package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Feature
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Component
import java.util.*

@Component
interface FeatureRepository : JpaRepository<Feature, Long> {

    fun findByIssue(issue: String): Optional<Feature>

    fun findFirstByName(name: String): Optional<Feature>

    fun findByArchivedAtNotNullOrderByArchivedAtDesc(pageable: Pageable): MutableList<Feature>

    fun findByArchivedAtNullOrderByCreatedAtDesc(): MutableList<Feature>

    @Query("select distinct f from Feature f inner join f.projectVersions pv inner join pv.project where f.archivedAt is null and pv.project.id=?1")
    fun findActiveByProject(projectId: Long): List<Feature>

    @Query("select distinct f from Feature f inner join f.projectVersions pv inner join pv.project where pv.project.id=?1")
    fun findAllByProject(projectId: Long): List<Feature>
}
