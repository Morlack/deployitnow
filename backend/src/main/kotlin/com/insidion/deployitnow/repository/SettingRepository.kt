package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Setting
import com.insidion.deployitnow.domain.entities.SettingName
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component

@Component
interface SettingRepository : JpaRepository<Setting, Long> {
    fun findByName(settingName: SettingName): Setting?
}
