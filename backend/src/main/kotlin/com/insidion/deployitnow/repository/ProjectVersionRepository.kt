package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.ProjectVersion
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Component
import java.util.*

@Component
interface ProjectVersionRepository : JpaRepository<ProjectVersion, Long> {
    @Query("select pv from ProjectVersion pv where pv.project.id=?1 and pv.number=?2")
    fun findByProjectAndNumber(projectId: Long, number: String): Optional<ProjectVersion>
}
