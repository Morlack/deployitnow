package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Project
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Component
import java.time.ZonedDateTime

@Component
interface ProjectRepository : JpaRepository<Project, Long> {
    fun findByGitlabId(id: Int): Project?

    fun findByHasEnvironmentsTrue(): List<Project>
    fun findByHasEnvironmentsFalse(): List<Project>
    fun findByHasEnvironmentsTrueAndLastCheckedBeforeOrderByLastCheckedAsc(afterLastChecked: ZonedDateTime, page: Pageable): List<Project>

    @Query("select max(p) from Project p where p not in (select t.project from ProjectRunningTask t where t.name=:type )")
    fun findByHasNeverHadTaskOfType(@Param("type") type: String, page: Pageable): List<Project>

    fun findAllByTeamNotNull(): List<Project>
}
