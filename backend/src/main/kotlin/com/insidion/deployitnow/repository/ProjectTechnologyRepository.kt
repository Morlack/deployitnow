package com.insidion.deployitnow.repository

import com.insidion.deployitnow.domain.entities.Project
import com.insidion.deployitnow.domain.entities.ProjectTechnology
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component

@Component
interface ProjectTechnologyRepository : JpaRepository<ProjectTechnology, Long> {
    fun findByProjectAndNameIn(project: Project, names: List<String>): List<ProjectTechnology>
}
