const fs = require('fs');
const https = require('https')

const blacklist = ["target", "node_modules"]

const technologyMatches = {
    npm: ["package.json"],
    maven: ["pom.xml"],
    docker: ["Dockerfile"],
}

function detectForDirectory(path, matches) {
    matches = matches || [];
    const files = fs.readdirSync(path);
    files
        .filter(file => blacklist.indexOf(file) === -1)
        .forEach(fileName => {
            const file = path + "/" + fileName;
            if (fs.statSync(file).isDirectory()) {
                matches = detectForDirectory(file, matches);
            } else {
                Object.keys(technologyMatches).forEach(technology => {
                    const fileMatches = technologyMatches[technology];
                    if(fileMatches.indexOf(fileName) !== -1) {
                        matches.push({technology, file})
                    }
                })
            }
        })
    return matches;
}

const technologies = detectForDirectory('.');
console.log("Detected technologies: ", technologies);

// Low-level http so we don't need npm
const req = https.request({
    host: process.env.MICROMANAGE_HOST,
    port: process.env.MICROMANAGE_PORT || 443,
    path: '/api/tasks/detector/technology?projectId=' + process.env.MICROMANAGE_PROJECT_ID,
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    }
}, function(response) {
    var str = ''
    response.on('data', function (chunk) {
        str += chunk;
    });

    response.on('end', function () {
        console.log(str);
    });
});

req.write(JSON.stringify(technologies));
req.end();

console.log("Finished detector")
