const fs = require('fs');
const https = require('https')
const { execSync } = require("child_process");

const blacklist = ["target", "node_modules", "node", ".git"]
const fileUpgrades = []

function getDiff(before, after, type) {
    if(before == null) {
        return [];
    }

    return Object.keys(before).map(dep => {
        const versionBefore = before[dep];
        const versionAfter = after[dep];

        if(versionBefore !== versionAfter) {
            return {
                type: type,
                identifier: dep,
                currentVersion: versionBefore,
                availableVersion: versionAfter
            }
        }
        return null;
    }).filter(u => u !== null)
}

function runForPackageJson(dir) {
    const beforeFile = fs.readFileSync(dir + "/package.json")
    const beforePackage = JSON.parse(beforeFile.toString())

    execSync("ncu -u", {cwd: dir, stdio: "inherit"})
    execSync("npm i", {cwd: dir, stdio: "inherit"})

    const afterFile = fs.readFileSync(dir + "/package.json")
    const afterFileLock = fs.readFileSync(dir + "/package-lock.json")
    const afterPackage = JSON.parse(afterFile.toString())

    const upgrades = getDiff(beforePackage.dependencies, afterPackage.dependencies, "dependencies")
        .concat(getDiff(beforePackage.devDependencies, afterPackage.devDependencies, "devDependencies"))

    if(upgrades.length > 0) {
        fileUpgrades.push(
            {file: (dir !== "." ? dir : "") + "/package.json", fileContentBase64: afterFile.toString("base64"), upgrades},
            {file: (dir !== "." ? dir : "") + "/package-lock.json", fileContentBase64: afterFileLock.toString("base64"), upgrades: []}
        )
    }
}

function detectForDirectory(path, matches) {
    matches = matches || [];
    const files = fs.readdirSync(path);
    files
        .filter(file => blacklist.indexOf(file) === -1)
        .forEach(fileName => {
                const file = path + "/" + fileName;
                if (fs.statSync(file).isDirectory()) {
                    matches = detectForDirectory(file, matches);
                } else {
                    if (fileName === "package.json") {
                        runForPackageJson(path)
                    }
                }
            }
        )
    return matches;
}

detectForDirectory(".")

// Low-level http so we don't need npm
const req = https.request({
    host: process.env.MICROMANAGE_BASE_URL.replace(/(http|https):/, "").replace(/\//g, ''),
    port: process.env.MICROMANAGE_PORT || 443,
    path: '/api/tasks/dependency/result/' + process.env.MICROMANAGE_PROJECT_ID + "/npm",
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    }
}, function(response) {
    var str = ''
    response.on('data', function (chunk) {
        str += chunk;
    });

    response.on('end', function () {
        console.log(str);
    });
});

req.write(JSON.stringify(fileUpgrades));
req.end();

console.log("Finished scanner")
