package com.insidion.deployitnow.domain.service

import com.vdurmont.semver4j.Semver
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class FeatureServiceTest {
    @Test
    fun testSemver() {
        assertThat(Semver("2019.12.1").isLowerThan("2019.12.1.1")).isTrue()
    }
}
